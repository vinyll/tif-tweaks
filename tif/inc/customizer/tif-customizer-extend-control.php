<?php

if ( ! defined( 'ABSPATH' ) ) exit;

global $tif_path;

require_once $tif_path . 'class/customizer/Tif_Customize_Color_Control.php';
require_once $tif_path . 'class/customizer/Tif_Customize_Category_Dropdown_Control.php';
require_once $tif_path . 'class/customizer/Tif_Customize_Checkbox_Multiple_Control.php';
require_once $tif_path . 'class/customizer/Tif_Customize_Checkbox_Sortable_Control.php';
require_once $tif_path . 'class/customizer/Tif_Customize_Number_Multiple_Control.php';
require_once $tif_path . 'class/customizer/Tif_Customize_Layout_Control.php';
require_once $tif_path . 'class/customizer/Tif_Customize_Loop_Layout_Thumb_Ratio_Control.php';
require_once $tif_path . 'class/customizer/Tif_Customize_Loop_Layout_Thumb_Size_Control.php';
require_once $tif_path . 'class/customizer/Tif_Customize_Radio_Image_Control.php';
require_once $tif_path . 'class/customizer/Tif_Customize_Range_Multiple_Control.php';
require_once $tif_path . 'class/customizer/Tif_Customize_Select_Multiple_Control.php';
require_once $tif_path . 'class/customizer/Tif_Customize_Text_Editor_Control.php';
require_once $tif_path . 'class/customizer/Tif_Customize_Text_Sortable_Control.php';
require_once $tif_path . 'class/customizer/Tif_Customize_Box_Shadow_Control.php';
require_once $tif_path . 'class/customizer/Tif_Customize_Alert_Control.php';
require_once $tif_path . 'class/customizer/Tif_Customize_Heading_Control.php';
