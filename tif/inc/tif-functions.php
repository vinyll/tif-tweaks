<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO [tif_get_default description]
 * @param  [type]  $id      [description]
 * @param  boolean $type    [description]
 * @param  boolean $name    [description]
 * @return [type]           [description]
 *
 *
 * @since 1.0
 * @version 1.0
 */
function tif_get_default( $name = false, $id, $type = false ) {

	$setup =
		strpos( $name, ',') !== false
		? substr($name, strpos($name, ",") + 1)
		: $name ;

	$tif_setup_data	= 'tif_' . $setup . '_setup_data' ;
	$tif_default	= $tif_setup_data() ;

	if ( strpos( $id, ',') !== false ) {

		$id = explode( ',', $id );
		$tif_id = $tif_default[$id[0]][$id[1]];

	} else {

		$tif_id = $tif_default[ $id ];

	}

	$tif_value = ( empty( $tif_id ) ) ? false : $tif_id ;

	if ( ! $tif_value )
		return;

	$tif_value = tif_sanitize_output( $tif_value, $type );

	return $tif_value;

}

function tif_get_option( $name = false, $id, $type = false ) {

	if (     defined( 'TIF_CUSTOMISE_' . strtoupper( $name ) )
		&& ! constant( 'TIF_CUSTOMISE_' . strtoupper( $name ) )
		&& ! constant( 'TIF_CUSTOMISE_THEME' )
		)
		return tif_get_default( $name, $id, $type );

	$setup =
		strpos( $name, ',') !== false
		? substr($name, strpos($name, ",") + 1)
		: $name ;
	$tif_setup_data = 'tif_' . $setup . '_setup_data' ;

	if ( ! function_exists( $tif_setup_data ) )
		return;

	$is_theme_mods	 = false !== strpos( $name, 'theme_' ) ? true : false ;
	$tif_default	 = $tif_setup_data();
	// $tif_mod	 = $is_theme_mods ? get_theme_mod( 'tif_' . $name, array() ) : get_option( 'tif_' . $name, array() );

	if ( $is_theme_mods ) {

		$tif_mod	 = get_theme_mod( 'tif_' . $name, array() );

	} else {

		if ( strpos( $name, ',') !== false ) {
			$name	 = explode( ',', $name );
			$tif_mod = get_option( (string)$name[0] )[(string)$name[1]];

		} else {
			$tif_mod = get_option( 'tif_' . $name, array() );
		}

	}

	if ( strpos( $id, ',') !== false ) {
		$id				= explode( ',', $id );
		$tif_default	= $tif_default[$id[0]];
		$tif_mod		= isset( $tif_mod[$id[0]] ) ? $tif_mod[$id[0]] : array() ;
		$tif_options	= wp_parse_args( $tif_mod, $tif_default );
		$tif_value		= $tif_options[$id[1]];
	} else {
		$tif_default	= $tif_setup_data();
		if( is_array( $tif_default[$id] ) && ! empty( $tif_mod[$id] ) && tif_is_associative_array( $tif_default[$id] ) ) :
			$tif_options	= wp_parse_args( $tif_mod[$id], $tif_default[$id] );
			$tif_value		= $tif_options;
		else:
			$tif_options	= wp_parse_args( $tif_mod, $tif_default );
			$tif_value		= $tif_options[$id];
		endif;
	}

	if ( ! $tif_value )
		return;

	if ( class_exists( 'Polylang' ) && $type == "text" )
		$tif_value = pll__( $tif_value );

	$tif_value = tif_sanitize_output( $tif_value, $type );

	return $tif_value;

}

/**
 * [tif_is_associative_array description]
 * @param  [type]  $array [description]
 * @return boolean        [description]
 * @TODO
 */
function tif_is_associative_array( $array ) {

	if( array_keys( $array ) !== range( 0, count( $array ) - 1) )
		return true;

}

/**
 * TODO [tif_sanitize_output description]
 * @param  string  $tif_value [description]
 * @param  boolean $type      [description]
 * @return [type]             [description]
 *
 *
 * @since 1.0
 * @version 1.0
 */
function tif_sanitize_output( $tif_value = '', $type = false ) {

	switch ($type) {
		case 'key':
			$tif_value = tif_sanitize_key( $tif_value );
			break;

		case 'slug':
			$tif_value = tif_sanitize_slug( $tif_value );
			break;

		case 'filename':
			$tif_value = tif_sanitize_filename( $tif_value );
			break;

		case 'date':
			$tif_value = tif_sanitize_date( $tif_value );
			break;

		case 'time':
			$tif_value = tif_sanitize_time( $tif_value );
			break;

		case 'checkbox':
		case 'bool':
			$tif_value = tif_sanitize_checkbox( $tif_value );
			break;

		case 'multicheck':
			$tif_value = tif_sanitize_multicheck( $tif_value );
			break;

		case 'loop_layout':
			$tif_value = tif_sanitize_loop_layout( $tif_value );
			break;

		case 'length':
			$tif_value = tif_sanitize_length( $tif_value );
			break;

		case 'sortable':
			$tif_value = tif_sanitize_sortable( $tif_value );
			break;

		case 'multiurl':
			$tif_value = tif_sanitize_multiurl( $tif_value );
			break;

		case 'array':
			$tif_value = tif_sanitize_array( $tif_value );
			break;

		case 'text':
		case 'string':
			$tif_value = tif_sanitize_string( $tif_value, true );
			break;

		case 'textarea':
			$tif_value = tif_sanitize_textarea( $tif_value );
			break;

		case 'nohtml':
			$tif_value = tif_sanitize_nohtml( $tif_value, true );
			break;

		case 'html':
			$tif_value = tif_sanitize_html( $tif_value );
			break;

		case 'css':
			$tif_value = tif_sanitize_css( $tif_value );
			break;

		case 'float':
			$tif_value = tif_sanitize_float( $tif_value );
			break;

		case 'int':
			$tif_value = intval( $tif_value );
			break;

		case 'absint':
			$tif_value = absint( $tif_value );
			break;

		case 'range':
			$tif_value = tif_sanitize_number_range( $tif_value );
			break;

		case 'dropdown_pages':
			$tif_value = tif_sanitize_dropdown_pages( $tif_value );
			break;

		case 'dropdown_category':
			$tif_value = tif_sanitize_dropdown_category( $tif_value );
			break;

		case 'email':
			$tif_value = tif_sanitize_email( $tif_value );
			break;

		case 'hexcolor':
			$tif_value = tif_sanitize_hexcolor( $tif_value );
			break;

		case 'keycolor':
			$tif_value = tif_sanitize_keycolor( $tif_value );
			break;

		case 'array_keycolor':
			$tif_value = tif_sanitize_array_keycolor( $tif_value );
			break;

		case 'array_boxshadow':
			$tif_value = tif_sanitize_array_boxshadow( $tif_value );
			break;

		case 'image':
			$tif_value = tif_sanitize_image( $tif_value );
			break;

		case 'select':
		$tif_value = tif_sanitize_string( $tif_value );
		break;

		case 'url':
			$tif_value = tif_sanitize_url( $tif_value );
			break;

		default:
			$tif_value = $tif_value;
			break;
	}

	return $tif_value;

}

function tif_is_writable( $path = 'uploads', $dir = false ) {

	// DEBUG:
	// return false;

	if ( $path == 'uploads' ) :
		$path = wp_upload_dir()['basedir'] . '/test_writable';
		$dir  = true;
	endif;

	if ( $dir ) {

		if ( is_writable( dirname( $path ) ) )
			return true;

	} else {

		if ( is_writable( $path ) )
			return true;

	}

}

/**
 * [tif_get_min_suffix description]
 * @return [type] [description]
 */
function tif_get_min_suffix( $type = 'css' ) {

	if ( class_exists ( 'Themes_In_France' ) ) {

		$settings = tif_get_option( 'theme_assets', 'tif_assets_minify_enabled', 'multicheck' );

		switch ( $type ) {
			case 'js':
				$suffix = in_array( 'js', $settings ) ? '.min' : null ;
				break;

			default:
				$suffix = in_array( 'css', $settings ) ? '.min' : null ;
				break;
		}

	} else {

		$suffix = '.min';

	}

	$suffix = current_user_can( 'administrator' ) || defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? null : $suffix ;

	return $suffix;

}

/**
 * Limit string length
 *
 * @author Frederic Caffin
 * @link https://themesinfrance.fr/.../
 *
 * @param $string
 * @param $limit
 *
 * @return $string
 */
/**
 * Cut out a string of characters
 * @param  [type]  $string
 * @param  boolean $length
 * @param  string  $more       True to display the more tag
 * @param  boolean $more_count If true, the more tag will be included in the breakdown
 * @return [type]
 * àTODO
 */
function tif_trim_charlength( $string, $length = false, $more = 'html', $more_count = false ) {

	if ( ! $length || $string == null )
		return $string;

	$string = trim( $string );

	$more_string = '&hellip;' ;
	switch ( $more ) {
		case 'text':
			$more = $more_string ;
			break;

		case 'html':
			$more = '<span class="ellipsis">' . $more_string . '</span>' ;
			break;

		default:
			$more = null;
			break;
	}

	// if ( (int)$length > strlen( $string ) )
	// 	return $string;

	if ( (int)$length > strlen( $more_string ) )
		$length = $more_count ? (int)$length - strlen( $more_string ) : (int)$length ;

	return trim( substr( (string)$string, 0, $length) ) . $more;

}
/**
 * Helper function to trim text using the same default values for length and
 * 'more' text as wp_trim_excerpt.
 * @TODO
 *
 * @link https://developer.wordpress.org/reference/functions/wp_trim_words/
 */
function tif_trim_words( $string, $length = false, $more = null ) {


	if ( ! $length || $string == null )
		return;

	$string = trim( $string );

	$more_string = '&hellip;' ;
	switch ( $more ) {
		case 'text':
			$more = $more_string ;
			break;

		case 'html':
			$more = '<span class="ellipsis">' . $more_string . '</span>' ;
			break;

		default:
			$more = null;
			break;
	}

	// $string = wp_trim_words( $string, (int)$length );

	// Replace <br> and <br /> by space
	$string = str_replace("<br>", " ", $string);
	$string = str_replace("<br/>", " ", $string);
	$string = str_replace("<br />", " ", $string);

	// Strip tags
	$string = strip_tags( $string );

	// Strip words
	$words  = str_word_count( $string, 2 );
	$pos    = array_keys( $words );

	if ( (int)$length < count( $words ) )
		$string = substr( $string, 0, $pos[$length] );

	$string = rtrim( $string, "." );
	$string = rtrim( $string, "," );

	return trim( $string ) . $more;

}

// /**
//  * [tif_trim_words_charlength description]
//  * @param  [type] $text       [description]
//  * @param  [type] $charlength [description]
//  * @return [type]             [description]
//  * @TODO
//  */
// function tif_trim_words_charlength( $text, $charlength ) {
//
// 	++$charlength;
//
// 	if ( mb_strlen( $text ) > $charlength ) {
//
// 		$subex    = mb_substr( $text, 0, $charlength );
// 		$exwords  = explode( ' ', $subex );
// 		$excut    = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
//
// 		if ( $excut < 0 ) {
// 			$text = mb_substr( $subex, 0, $excut );
// 		} else {
// 			$text = $subex;
// 		}
//
// 		return trim( $text ) . '<span class="ellipsis">&hellip;</span>';
//
// 	} else {
//
// 		return trim( $text );
//
// 	}
// }

/**
 * [tif_get_lazy_attr description]
 * @return [type] [description]
 */
function tif_get_lazy_attr() {

	$lazy = tif_get_option( 'theme_images', 'tif_lazy_enabled', 'checkbox' ) ? array( 'loading' => 'lazy' ) : array() ;

	return $lazy;

}

/**
 * TODO
 * @param  [type] $attr [description]
 * @return [type]       [description]
 */
function tif_parse_attr( $attr = null, $additional = array() ) {

	if ( ! is_array( $attr ) )
		return;

	$attr = wp_parse_args( $attr );

	$args = '' ;
	foreach ( $attr as $key => $value ) {

		if ( is_array( $value ) )
			$value = implode( ' ', $value );

		$args .= ' '. esc_html( $key ) . '="' . esc_html( $value ) . ( isset( $additional[$key] ) ? ' ' . esc_html( $additional[$key] ) : null ) . '"';
	}

	return $args;

}

// /**
//  * [tif_replace_array_key description]
//  * @param  [type] $array   [description]
//  * @param  [type] $old_key [description]
//  * @param  [type] $new_key [description]
//  * @return [type]          [description]
//  * TODO
//  */
// function tif_replace_array_key( $array, $old_key, $new_key ) {
//
// 	$keys = array_keys( $array );
//
// 	// DEBUG
// 	if ( false === $index = array_search( $old_key, $keys, true ) ) {
// 		throw new Exception( sprintf( 'Key "%s" does not exist', $old_key ) );
// 	}
//
// 	$keys[$index] = $new_key;
//
// 	return array_combine( $keys, array_values( $array ) );
// }

/**
 * Replaces an array key and preserves the original
 * order.
 *
 * @param $array The array in question.
 * @param $old_key The key that you want to replace.
 * @param $new_key The name of the new key.
 *
 * @return array
 */
function tif_replace_array_key( $array, $old_key, $new_key ) {

	// If the old key doesn't exist, we can't replace it...
	if( ! array_key_exists( $old_key, $array ) )
		return $array;

	// Get a list of all keys in the array
	$array_keys = array_keys( $array );

	// Replace the key in our $array_keys array
	$old_key_index = array_search( $old_key, $array_keys );
	$array_keys[$old_key_index] = $new_key;

	// Combine them back into one array
	$newArray =  array_combine( $array_keys, $array );

	return $newArray;

}

/**
 * Like wp_parse_args but supports recursivity
 * By default converts the returned type based on the $args and $defaults
 *
 * @param  array|object $args                   Values to merge with $defaults.
 * @param  array|object $defaults               Array, Object that serves as the defaults or string.
 * @param  boolean      $preserve_type          Optional. Convert output array into object if $args or $defaults if it is. Default true.
 * @param  boolean      $preserve_integer_keys  Optional. If given, integer keys will be preserved and merged instead of appended.
 *
 * @return array|object  $output                 Merged user defined values with defaults.
 *
 * @source https://github.com/kallookoo/wp_parse_args_recursive/blob/master/src/wp-parse-args-recursive.php
 * @link https://developer.wordpress.org/reference/functions/wp_parse_args/#comment-2556
 */
function tif_parse_args_recursive( $args, $defaults, $preserve_type = true, $preserve_integer_keys = false ) {
	$output = array();
	foreach ( array( $defaults, $args ) as $list ) {
		foreach ( (array) $list as $key => $value ) {
			if ( is_integer( $key ) && ! $preserve_integer_keys ) {
				$output[] = $value;
			} elseif (
				isset( $output[ $key ] ) &&
				( is_array( $output[ $key ] ) || is_object( $output[ $key ] ) ) &&
				( is_array( $value ) || is_object( $value ) )
			) {
				$output[ $key ] = tif_parse_args_recursive( $value, $output[ $key ], $preserve_type, $preserve_integer_keys );
			} else {
				$output[ $key ] = $value;
			}
		}
	}
	return ( $preserve_type && ( is_object( $args ) || is_object( $defaults ) ) ) ? (object) $output : $output;
}

/**
 * [tif_parse_args_recursive description]
 * @param  array  $attr               [description]
 * @return [type]       [description]
 * @TODO
 */
function tif_parse_loop_attrs( $attr = array(), $defaults = array() ) {

	$output = $defaults;

	foreach ( $attr as $key => $value ) {

		if ( $key == 'additional' && isset( $output[$key] ) && is_array( $output[$key] ) ) {

			$output[$key] = array_merge_recursive( $output[$key], $value );
			foreach ( $output[$key] as $additional_key => $additional_value ) {

				if ( is_array( $output[ 'additional'][$additional_key] ) )
					$output['additional'][$additional_key] = implode( ' ', $additional_value );

			}

		} elseif ( is_array( $value ) && isset( $output[$key] ) && is_array( $output[$key] ) ) {

			$output[$key] = tif_parse_loop_attrs( $output[$key], $value );

		} else {

			$output[$key] = $value;

		}

	}

	return $output;

}

/**
 * [tif_array_merge_value_recursive description]
 * @param  array  $array               [description]
 * @return [type]        [description]
 * @TODO
 */
function tif_array_merge_value_recursive( $array = array() ) {

	if ( empty( $array ) )
		return;

	$val = array();
	foreach ( $array as $key => $value ) {

		$value = is_array( $value ) ? $value : explode( ',', $value );
		if( ! empty( $value ) && null != $value[0] ) {
			$val = array_merge_recursive( $val, $value );
		}

	}

	return ( ! empty( $val ) ? $val : array() );

}

function tif_get_translated ( $string ) {

	if ( class_exists( 'Polylang' ) ) {

		$string = pll__( $string );

	}

	return (string)$string;

}

function tif_get_wp_roles(){
	global $wp_roles;

	if (!isset( $wp_roles))
		$wp_roles = new WP_Roles();

	$roles = $wp_roles->get_names();
	$roles_translated = array();

	foreach ( $roles as $key => $value ) {

		if ( $key != 'administrator' )
		$roles_translated[$key] = translate_user_role( $value );

	}

	return $roles_translated;
}

/**
 * Checks if the current visitor is a logged in user.
 * @return boolean True if user is logged in, false if not logged in.
 */
function tif_is_user_logged_in() {

	$user = wp_get_current_user();

	if ( empty( $user->ID ) )
		return false;

	else
		return true;

}

function tif_get_current_user_role() {

	$tif_current_user_role = null;

	if ( tif_is_user_logged_in() ) :

		$tif_user_id   = get_current_user_id();
		$tif_user_info = get_userdata( $tif_user_id );

		if ( $tif_user_info )
			$tif_current_user_role = implode( ', ', $tif_user_info->roles);

	endif;

	return $tif_current_user_role;

}

function tif_current_user_can( $value ) {

	if ( ! $value || ! is_array( $value ))
		return;

	$tif_current_user_can = false;

	if ( in_array( tif_get_current_user_role(), $value ) )
		$tif_current_user_can = true;

	return (bool)$tif_current_user_can;

}

function tif_get_current_user_capability( $value ) {

	$tif_current_user_role = tif_get_current_user_role();
	$capability = 'manage_options';

	if ( tif_current_user_can( $value ) ) :
		switch ( $tif_current_user_role ) {
			case 'editor':
				$capability = 'edit_posts';
				break;

			case 'contributor':
				$capability = 'edit_posts';
				break;

		}
	endif;

	return (string)$capability;

}

function tif_allowed_html() {

	$array = array(
		'a' => array(
			'href' => array(
				'javascript:',
			),
			'title' => array(),
			'target' => array(),
			'rel' => array(),
			'id' => array(),
			'class' => array()
		),
		'img' => array(
			'src' => array(),
			'id' => array(),
			'class' => array()
		),
		'div' => array(
			'id' => array(),
			'class' => array(),
		),
		'i' => array(),
		'u' => array(),
		'p' => array(
			'class' => array()
		),
		'br' => array(),
		'em' => array(),
		'small' => array(),
		'span' => array(
			'class' => array(),
		),
		'strong' => array(),
		'sup' => array(
			'class' => array(),
		),
	);

	return $array;

}

/**
 * Parse css class
 * @TODO
 * @param  [type] $cb    [description]
 * @param  string $class [description]
 * @return [type]        [description]
 */
function tif_parse_string( $class = '', $cb ) {

	if ( ! $cb )
		return;

	$classes = array();

	if ( ! empty( $class ) ) {
		if ( ! is_array( $class ) ) {
			$class = preg_split( '#\s+#', $class );
		}
		$classes = array_merge( $classes, $class );
	} else {
		// Ensure that we always coerce class to being an array.
		$class = array();
	}

	$classes = array_map( 'esc_attr', $classes );
	$classes = apply_filters( 'tif_' . $cb, $classes, $class );

	return array_unique( $classes );

}

/**
 * [tif_get_scss_length_array description]
 * @return [type] [description]
 * @TODO
 */
function tif_get_scss_length_array( $key = false ) {

	// Previously in tif_theme_utils_setup_data()
	$lengths = array(
		'length'				=> array(
			'tif_length_small'		=> '.625,rem',
			'tif_length_medium'		=> '1.25,rem',
			'tif_length_large'		=> '1.875,rem',
		),
		'spacer'				=> array(
			'spacer_none'			=> '0',			// none
			'spacer_tiny'			=> '.125,rem',	// 2
			'spacer_tiny_plus'		=> '.313,rem',	// 5
			'spacer_small'			=> '.5,rem',	// 8
			'spacer_small_plus'		=> '.625,rem',	// 10
			'spacer_medium'			=> '1,rem',		// 16
			'spacer_medium_plus'	=> '1.25,rem',	// 20
			'spacer_large'			=> '1.5,rem',	// 24
			'spacer_large_plus'		=> '2.25,rem',	// 36

		),
		'gap'					=> array(
			'gap_none'				=> '0',			// none
			'gap_tiny'				=> '.313,rem',	// 5
			'gap_small'				=> '.625,rem',	// 10
			'gap_medium'			=> '1,rem',		// 16
			'gap_large'				=> '1.25,rem',	// 20
		),
		'border_radius'			=> array(
			'radius_none'			=> '0',			// none
			'radius_tiny'			=> '.125,rem',	// 2
			'radius_small'			=> '.5,rem',	// 8
			'radius_medium'			=> '1,rem',		// 16
			'radius_large'			=> '2,rem',		// 32
			'radius_circle'			=> '50,%',		// circle
		),
	);

	$lengths = call_user_func_array( 'array_merge_recursive', $lengths);

	if ( $key )
		return array_keys( $lengths );

	else
		return $lengths;

}

/**
 * [tif_get_scss_length_variable description]
 * @TODO
 */
function tif_get_scss_length_variable( $variable = null ) {

	if ( null == $variable )
		return;

	$scss_array = tif_get_scss_length_array( true );
	$length		= false;

	foreach ( $variable as $key => $value ) {

		if( array_key_exists( (string)$value, $scss_array ) ) {
			$length .= '$' . tif_sanitize_slug( $value ) . ' ';
		}

	}

	if ( ! $length )
		return false;

	return trim( $length );

}

/**
 * [tif_get_length_value description]
 * @TODO
 */
function tif_get_length_value( $dimension, $unit = 'px', $min = 0 ) {

	$dimension = ! is_array( $dimension ) ? explode( ',', $dimension ) : $dimension;

	if ( empty( $dimension ) )
		return false;

	$scss_array = tif_get_scss_length_array();
	$unit_array = array(
		'px',
		'pt',
		'em',
		'rem',
		'%',
		'vh',
		'vw',
	);

	$unit   = in_array( end( $dimension ), $unit_array  ) ? array_pop( $dimension ) : $unit ;
	$length = null;

	foreach ( $dimension as $key => $value ) {
		if( array_key_exists( (string)$value, $scss_array ) ) {
			$length .= tif_get_length_value( $scss_array[$value] ). ' ';
		} else {
			$length .= $value != 0 ? (float)$value . $unit . ' ' : '0 ';
		}

	}

	// if ( $length == '0 ' )
	// 	return false ;

	return trim( $length );

}

/**
 * [tif_get_box_declarations description]
 * @param  [type] $name [description]
 * @param  [type] $id   [description]
 * @param  string $unit [description]
 * @return [type]       [description]
 * @TODO
 */
function tif_get_box_declarations( $name, $id, $unit = 'px' ) {

	$box	= tif_get_option( $name, $id, 'array' );

	if ( empty( $box ) )
		return false;

	foreach ( $box as $key => $value ) {

		if ( $key == 'box_shadow' ) {

			$value = tif_sanitize_array_boxshadow( $value );

			if ( (float)$value[5] > 0 ) {
				$custom_colors	= new Tif_Custom_Colors;
				$box[$key] = (float)$value[0] . 'px '		// position x
							. (float)$value[1] . 'px ' 		// position y
							. (float)$value[2] . 'px ' 		// blur
							. (float)$value[3] . 'px ' 		// spread
							. $custom_colors->tif_get_color_from_key( array( (string)$value[4], 'normal', (float)$value[5] ) ) . ' ' 		// color and opacity
							. (string)$value[6]; 			// inset
			} else {
				$box[$key] = false;
			}

		} else {

			$box[$key] = tif_get_length_value( $value ) ; ;

		}

	}

	return (array)$box;

}

/**
 * [tif_get_font_declarations description]
 * @param  [type] $name [description]
 * @param  [type] $id   [description]
 * @return [type]       [description]
 * @TODO
 */
function tif_get_font_declarations( $name, $id ) {

	$fonts_value	= tif_get_option( $name, $id, 'array' );
	$array			= tif_replace_array_key( $fonts_value, 'font_stack', 'font_family' );

	if ( isset( $array['font_family'] ) )
		$array['font_family'] = tif_get_font_stack( $array['font_family'] );

	if ( isset( $array['font_size'] ) ) {

		if (   $id == 'tif_tiny_font'
			|| $id == 'tif_small_font'
			|| $id == 'tif_normal_font'
			|| $id == 'tif_medium_font'
			|| $id == 'tif_large_font'
			|| $id == 'tif_extra_large_font'
			|| $id == 'tif_huge_font'
			|| $id == 'tif_heading_font'
			|| $id == 'tif_branding_font'
		) {
			$font_size = tif_sanitize_multicheck( $array['font_size'] );
			$i = 0;
			foreach ( $font_size as $k ) {

				if ( $id != 'tif_heading_font' ) {
					switch ( $i ) {
						case 1:
							$screen = 'lg_';
							break;

						case 2:
							$screen = 'sm_';
							break;

						default:
							$screen = null;
							break;
					}
				} else {
					$screen = 'h' . ( $i + 1 ) . '_';
				}

				$array[$screen . 'font_size'] = $k > 0 ? (float)$k . 'rem' : null ;
				++$i;
			}

		// } elseif ( $array['font_size'] == 'font_size_small'
		// 		|| $array['font_size'] == 'font_size_tiny'
		// 		|| $array['font_size'] == 'font_size_normal'
		// 		|| $array['font_size'] == 'font_size_large'
		// 		|| $array['font_size'] == 'font_size_extra_large'
		// 		|| $array['font_size'] == 'font_size_medium'
		// 		|| $array['font_size'] == 'font_size_huge'
		// 	) {}

		} elseif ( strpos( $array['font_size'], 'font_size') !== false ) {

			$array['font_size'] = 'var(--tif' . tif_esc_css( str_replace( 'font_size', '', $array['font_size'] ) ) . '-font-size)';

		}

	}

	if ( isset( $array['letter_spacing'] ) )
		$array['letter_spacing'] = null != $array['letter_spacing'] ? (int)$array['letter_spacing'] . 'px' : false ;

	if ( $id == 'tif_heading_font' )
		unset($array['font_size']);

	return (array)$array;
}

/**
 * [tif_get_alignment_declarations description]
 * @param  [type] $name [description]
 * @param  [type] $id   [description]
 * @return [type]       [description]
 * @TODO
 */
function tif_get_alignment_declarations( $name, $id ) {

	$array	= tif_get_option( $name, $id, 'array' );

	if ( isset( $array['flex_basis'] ) )
		$array['flex_basis']	= tif_get_length_value( $array['flex_basis'] ) ;

	if ( isset( $array['gap'] ) )
		$array['gap']	= tif_get_length_value( $array['gap'] ) ;

	if ( isset( $array['grid_gap'] ) )
		$array['grid_gap']	= tif_get_length_value( $array['grid_gap'] ) ;

	return (array)$array;

}

/**
 * [tif_get_scss_variables description]
 * @param  [type]  $name  [description]
 * @param  [type]  $array [description]
 * @param  boolean $cb    [description]
 * @return [type]         [description]
 * @TODO
 */
function tif_get_scss_variables( $name, $array, $cb = false ) {

	$variables = array();

	foreach ( $array as $key => $value ) {

		$value = null != $value ? $value : false ;

		if (    $key == 'vertical_padding'
			||  $key == 'horizontal_padding'
			||  $key == 'vertical_margin'
			||  $key == 'horizontal_margin' ) {

			$key	= explode( '_', $key );
			$value	= explode( ' ', $value );

			$variables = array_merge(
				$variables,
				array(
					tif_esc_css( '$' . $name . '-' . $key[1] . ( $key[0] == 'vertical' ? '-top' : '-left' ) ) => tif_esc_css( $value[0] ),
					tif_esc_css( '$' . $name . '-' . $key[1] . ( $key[0] == 'vertical' ? '-bottom' : '-right' ) ) => tif_esc_css( isset( $value[1] ) ? $value[1] : $value[0] ),
				)
			);

		} else {

			$variables = array_merge(
				$variables,
				array( tif_esc_css( '$' . $name . '-' . $key ) => tif_esc_css( $value ) )
			);

		}

	}

	if ( ! empty( $variables ) && $cb && count( array_filter($variables) ) > 0 )
		$variables = array_merge( array( tif_esc_css( '$' . $name ) => true ), $variables );

	return $variables;

}

/**
 * [tif_plugin_help description]
 * @return [type] [description]
 * @TODO
 */
function tif_plugin_help() {

	/**
	 * @see https://www.w3schools.com/charsets/ref_emoji_smileys.asp
	 */

	$powered ='<div>
		' . sprintf(
			'<a href="%s" target="_blank" rel="noreferrer noopener"><img src="%sassets/img/logo_tif_400px.svg' . '" /></a>',
			esc_url( 'https://themesinfrance.fr/' ),
			Tif_Init::tif_get_tif_url()
		)
		. '
	<p>
		' . sprintf(
			'%s <a href="%s" target="_blank" rel="noreferrer noopener">%s</a> %s',
			esc_html__( 'Soon on', 'canopee' ),
			esc_url( 'https://themesinfrance.fr/wordpress/plugins/' ),
			esc_html__( 'Themes in France', 'canopee' ),
			'&#128515;'
		)
		. '
	</p></div>
	';

	return $powered;

}

/**
 * [tif_plugin_powered_by description]
 * @return [type] [description]
 * TODO
 */
function tif_plugin_powered_by() {

	$powered ='
	<p>
		' . sprintf(
			'%s <a href="%s" target="_blank" rel="noreferrer noopener">%s</a>',
			esc_html__( 'We are very happy to provide this and other', 'canopee' ),
			esc_url( 'https://themesinfrance.fr/wordpress/plugins/' ),
			esc_html__( 'free WordPress plugins', 'canopee' )
		)
		. '
	<br />
		' . sprintf(
			'%s <a href="%s" target="_blank" rel="noreferrer noopener">%s</a>',
			esc_html__( 'Plugin developed by', 'canopee' ),
			esc_url( 'https://themesinfrance.fr' ),
			esc_html__( 'Themes in France', 'canopee' )
		)
		. '
	</p>
	';

	return $powered;

}

/**
 * [tif_memory_usage description]
 * @return [type] [description]
 * @TODO
 */
function tif_memory_usage() {

	$memory ='
	<small>
		' . sprintf(
			'%s : %s',
			esc_html__( 'Memory usage', 'canopee' ),
			tif_convert_bytes( memory_get_usage( true ) )
		)
		. '
	</small>
	';

	return $memory;

}

/**
 * [tif_convert_bytes description]
 * @param  [type] $size [description]
 * @return [type]       [description]
 * @TODO
 */
function tif_convert_bytes( $size ) {

	$unit = array(
		'b',
		'kb',
		'mb',
		'gb',
		'tb',
		'pb'
	);

	return @round( $size/pow( 1024,( $i=floor( log( $size,1024 ) ) ) ),2 ).' '.$unit[$i];

}
