<?php

/**
 * Used to avoid generating a blank image each time we try to change an image ratio in the customizer
 * return blank png
 */

if ( ! isset($_GET['w']) || ! isset($_GET['h']) )
	return;

$im		= imagecreatetruecolor( intval($_GET['w']), intval($_GET['h']) );
$black	= imagecolorallocate( $im, 0, 0, 0 );

// We make the background transparent
imagecolortransparent( $im, $black );

header( 'Content-Type: image/png' );
imagepng( $im );
imagedestroy( $im );
