jQuery( document ).ready( function($) {

	function tifGetColorBrightness(value){

		switch(value) {
			case "-60":
				return "lightest"
			break;
			case "-40":
				return "lighter"
			break;
			case "-20":
				return "light"
			break;
			case "0":
				return "normal"
			break;
			case "20":
				return "dark"
			break;
			case "40":
				return "darker"
			break;
			case "60":
				return "darkest"
			break;
			default:
				return "normal"
		}

	}

	// Check all checkbox
	$('input.checkall[type=checkbox]').click(function (event) {
		var checked = $(this).is(':checked');
		if (checked) {
			$(this).parent().find('input[type=checkbox]').prop('checked', true);
		} else {
			$(this).parent().find('input[type=checkbox]').prop('checked', false);
		}
	});

	$(document).ready(function () {
		$('ul.tif-sortable').sortable({
			axis: 'y',
			cursor: 'grabbing',
			update: function( e, ui ){
				$('ul.tif-sortable li input').trigger( 'change' );
			}
		});
	});

	/* === Tif_Customize_Color_Control for key array === */
	$( 'ul.tif-key-array-color input[type="radio"], ul.tif-key-array-color input[type="range"]' ).change(function() {

		color = $(this).parents( 'ul.tif-key-array-color' ).find( 'input[type="radio"]:checked' ).val();
		if ($(this).parents( 'ul.tif-key-array-color' ).find('input[class="tif-brightness-input-range"]').length > 0) {
			brightness = $(this).parents( 'ul.tif-key-array-color' ).find('input[class="tif-brightness-input-range"]').val();
		} else {
			brightness = 'normal';
		}
		if ($(this).parents( 'ul.tif-key-array-color' ).find('input[class="tif-opacity-input-range"]').length > 0) {
			opacity = $(this).parents( 'ul.tif-key-array-color' ).find('input[class="tif-opacity-input-range"]').val();
		} else {
			opacity = '1';
		}

		$(this).parents( 'ul.tif-key-array-color' ).find( 'input[type="hidden"]' ).val( color + ',' + tifGetColorBrightness(brightness) + ',' + opacity ).trigger( 'change' );

	});

	/* === Tif_Customize_Color_Control for hex array === */
	$( 'ul.tif-hex-array-color input[type="text"], ul.tif-hex-array-color input[type="number"]' ).change(function() {

		color = $(this).parents( 'ul.tif-hex-array-color' ).find( 'input[type="text"]' ).val();
		if ($(this).parents( 'ul.tif-hex-array-color' ).find( 'input[type="number"]' ).length > 0) {
			opacity = $(this).parents( 'ul.tif-hex-array-color' ).find( 'input[type="number"]' ).val();
		} else {
			opacity = '1';
		}

		$(this).parents( 'ul.tif-hex-array-color' ).find( 'input[type="hidden"]' ).val( color + ',normal,' + opacity ).trigger( 'change' );

	});

	/* === Tif_Customize_Box_Shadow_Control === */
	$( 'ul.tif-key-box-shadow input[type="radio"], ul.tif-key-box-shadow input[type="text"], ul.tif-key-box-shadow input[type="checkbox"], ul.tif-key-box-shadow input[type="range"]' ).change(function() {

		if ($(this).parents( 'ul.tif-key-box-shadow' ).find( 'input[type="text"]' ).length > 0) {
			color = $(this).parents( 'ul.tif-key-box-shadow' ).find( 'input[type="text"]' ).val();
		} else {
			color = $(this).parents( 'ul.tif-key-box-shadow' ).find( 'input[type="radio"]:checked' ).val();
		}
		positionx = $(this).parents( 'ul.tif-key-box-shadow' ).find('input[class="tif-position-x-input-range"]').val();
		positiony = $(this).parents( 'ul.tif-key-box-shadow' ).find('input[class="tif-position-y-input-range"]').val();
		blur = $(this).parents( 'ul.tif-key-box-shadow' ).find('input[class="tif-blur-input-range"]').val();
		spread = $(this).parents( 'ul.tif-key-box-shadow' ).find('input[class="tif-spread-input-range"]').val();
		opacity = $(this).parents( 'ul.tif-key-box-shadow' ).find('input[class="tif-opacity-input-range"]').val();
		inset = $(this).parents( 'ul.tif-key-box-shadow' ).find('input[type="checkbox"]').is(':checked');
		if (inset) {
			inset = 'inset';
		} else {
			inset = '';
		}

		$(this).parents( 'ul.tif-key-box-shadow' ).find( 'input[type="hidden"]' ).val( positionx + ',' + positiony + ',' + blur + ',' + spread + ',' + color + ',' + opacity + ',' + inset ).trigger( 'change' );

	});

	/* === Tif_Customize_Number_Multiple_Control === */
	$( 'ul.tif-multinumber input[type="number"], ul.tif-multinumber select' ).bind( 'change', function() {

		number_values = $(this).parents( 'ul.tif-multinumber ' ).find( 'input[type="number"]' ).map(
			function() {
				return this.value;
			}
		).get().join(',');
		unity = $(this).parents( 'ul.tif-multinumber ' ).find( 'select' ).val();
		unity = (unity != undefined) ? ',' + unity : '';

		$(this).parents( 'ul.tif-multinumber ' ).find( 'input[type="hidden"]' ).val( number_values + '' + unity ).trigger( 'change' );

	});

	/* === Tif_Loop_Layout_Control === */
	$( 'ul.tif-layout-control input[type="radio"]' ).on( 'change', function() {
		tifChangeLayoutSettings( this );
	});

	$( 'ul.tif-layout-control input[type="text"]' ).on( 'input', function() {
		tifChangeLayoutSettings( this );
	});

	function tifChangeLayoutSettings(value){

		var tmp = [];

		tmp.push(jQuery( value ).parents( 'ul.tif-layout-control' ).find('input[type="radio"]:checked').val());
		tmp.push(jQuery( value ).parents( 'ul.tif-layout-control' ).find('input[type=text]').map(function(){
			return this.value;
		}).get().join(','));

		jQuery( value ).parents( 'ul.tif-layout-control' ).find( 'input[type="hidden"]' ).val( tmp.join(',') ).trigger( 'change' );

	}

	/* === Tif_Loop_Layout_Control === */
	$( 'ul.tif-loop-layout-control input[type="radio"], ul.tif-loop-layout-control input[type="number"], ul.tif-loop-layout-control select' ).on( 'change', function() {
		tifChangeLoopLayoutSettings( this );
	});

	$( 'ul.tif-loop-layout-control input[type="text"]' ).on( 'input', function() {
		tifChangeLoopLayoutSettings( this );
	});

	function tifChangeLoopLayoutSettings(value){

		var tmp = [];
		var inputs = [
			'input[type="radio"]:checked',
	   		'input[type="number"]',
	   		'select',
	   		'input.container-class[type="text"]',
	   		'input.post-class[type="text"]'
		];

		$.each(inputs, function(index, input) {
				tmp.push(jQuery( value ).parents( 'ul.tif-loop-layout-control' ).find( input ).val())
		});

		jQuery( value ).parents( 'ul.tif-loop-layout-control' ).find( 'input[type="hidden"]' ).val( tmp.join(',') ).trigger( 'change' );

	}

	/* === Tif_Customize_Checkbox_Multiple_Control === */
	$( 'ul.tif-multicheck input[type="checkbox"]' ).on( 'change', function() {

		checkbox_values = $(this).parents( 'ul.tif-multicheck' ).find( 'input:not(".checkall")[type="checkbox"]:checked' ).map(
			function() {
				return this.value;
			}
		).get().join(',');

		$(this).parents( 'ul.tif-multicheck' ).find( 'input[type="hidden"]' ).val( checkbox_values ).trigger( 'change' );

	});

	/* === Tif_Customize_Checkbox_Sortable_Control === */
	$( 'ul.tif-multicheck-sortable input[type="checkbox"]' ).on( 'change', function() {

		sortable_values = $(this).parents( 'ul.tif-multicheck-sortable' ).find( 'input[type="checkbox"]' ).map( function() {
			var active = '0';
			if( $(this).prop("checked") ){
				active = '1';
			}
			return this.value + ':' + active;
		}).get().join(',');

		$(this).parents( 'ul.tif-multicheck-sortable' ).find( 'input[type="hidden"]' ).val( sortable_values ).trigger( 'change' );

	});


	/* === Tif_Customize_Text_Sortable_Control === */
	$( 'ul.tif-multitext-sortable input[type="text"]' ).on( 'change', function() {

		sortable_values = $(this).parents( 'ul.tif-multitext-sortable' ).find( 'input[type="text"]' ).map( function() {
			var active = null;
			if( $(this).val().length != 0 ){
				active = $(this).attr('name') + ':' + encodeURIComponent($(this).val());
			}
			return active;
		}).get().join(',');

		$(this).parents( 'ul.tif-multitext-sortable' ).find( 'input[type="hidden"]' ).val( sortable_values ).trigger( 'change' );

	});

	/* === Tif_Customize_Number_Multiple_Control === */
	$( 'ul.tif-multinumber input[type="number"], ul.tif-multinumber select' ).bind( 'change', function() {

		number_values = $(this).parents( 'ul.tif-multinumber ' ).find( 'input[type="number"]' ).map(
			function() {
				return this.value;
			}
		).get().join(',');
		unity = $(this).parents( 'ul.tif-multinumber ' ).find( 'select' ).val();
		unity = (unity != undefined) ? ',' + unity : '';

		$(this).parents( 'ul.tif-multinumber ' ).find( 'input[type="hidden"]' ).val( number_values + '' + unity ).trigger( 'change' );

	});

	/* === Tif_Customize_Select_Multiple_Control === */
	$( 'ul.tif-multiselect select' ).bind( 'change', function() {

		var values = new Array();
		var select_values;
		$.each($(this).parents( 'ul.tif-multiselect' ).find( ':selected' ), function() {
			values.push($(this).val());
		});
		select_values = values.join(',');

		$(this).parents( 'ul.tif-multiselect' ).find( 'input[type="hidden"]' ).val( select_values ).trigger( 'change' );

		});

	/* === Tif_Customize_Range_Multiple_Control === */
	$( 'ul.tif-multirange input[type="range"]' ).on( 'change', function() {

		range_values = $(this).parents( 'ul.tif-multirange' ).find( 'input[type="range"]' ).map(
			function() {
				return this.value;
			}
		).get().join(',');

		$(this).parents( 'ul.tif-multirange' ).find( 'input[type="hidden"]' ).val( range_values ).trigger( 'change' );

	});

} ); // jQuery( document ).ready

/* === Tif_Customize_Text_Editor_Control === */
jQuery( document ).ready(function($) {
	"use strict";
	$('.customize-control-tinymce-editor').each(function(){
		// Get the toolbar strings that were passed from the PHP Class
		var tinyMCEToolbar1String = _wpCustomizeSettings.controls[$(this).attr('id')].tif_tinymce_toolbar1;
		var tinyMCEToolbar2String = _wpCustomizeSettings.controls[$(this).attr('id')].tif_tinymce_toolbar2;
		var tinyMCEMediaButtons = _wpCustomizeSettings.controls[$(this).attr('id')].tif_media_buttons;

		wp.editor.initialize( $(this).attr('id'), {
			tinymce: {
				wpautop: true,
				toolbar1: tinyMCEToolbar1String,
				toolbar2: tinyMCEToolbar2String
			},
			quicktags: true,
			mediaButtons: tinyMCEMediaButtons
		});
	});
	$(document).on( 'tinymce-editor-init', function( event, editor ) {
		editor.on('change', function(e) {
			tinyMCE.triggerSave();
			$('#'+editor.id).trigger('change');
		});
	});
}); // jQuery( document ).ready
