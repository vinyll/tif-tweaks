<?php

if ( ! defined( 'ABSPATH' ) ) exit;

// v 0.8.6
// @link https://github.com/joshcanhelp/php-form-builder/blob/master/PhpFormBuilder.php

class Tif_Form_Builder {

	// Stores all form inputs
	private $inputs = array();

	// Stores all form attributes
	private $form = array();

	// Does this form have a submit value?
	private $has_submit = false;

	/**
	 * Constructor function to set form action and attributes
	 *
	 * @param string $action
	 * @param bool   $args
	 */
	function __construct( $action = '', $args = false ) {

		// Default form attributes
		$defaults = array(
			'action'		=> $action,
			'method'		=> 'post',
			'enctype'		=> 'application/x-www-form-urlencoded',
			'class'			=> array(),
			'id'			=> '',
			'markup'		=> 'html',
			'novalidate'	=> false,
			'add_nonce'		=> false,
			'add_honeypot'	=> false,
			'is_array'		=> '',
			'form_element'	=> true,
			'wrap_input'	=> false,
			'add_submit'	=> true,
			'text_submit'	=> 'Save changes'
		);

		// Merge with arguments, if present
		if ( $args ) {
			$settings = array_merge( $defaults, $args );
		} // Otherwise, use the defaults wholesale
		else {
			$settings = $defaults;
		}

		// Iterate through and save each option
		foreach ( $settings as $key => $val ) {
			// Try setting with user-passed setting
			// If not, try the default with the same key name
			if ( ! $this->set_att( $key, $val ) ) {
				$this->set_att( $key, $defaults[ $key ] );
			}
		}
	}

	/**
	 * Validate and set form
	 *
	 * @param string        $key A valid key; switch statement ensures validity
	 * @param string | bool $val A valid value; validated for each key
	 *
	 * @return bool
	 */
	function set_att( $key, $val ) {

		switch ( $key ) :

			case 'action':
				break;

			case 'method':
				if ( ! in_array( $val, array( 'post', 'get' ) ) ) {
					return false;
				}
				break;

			case 'enctype':
				if ( ! in_array( $val, array( 'application/x-www-form-urlencoded', 'multipart/form-data' ) ) ) {
					return false;
				}
				break;

			case 'markup':
				if ( ! in_array( $val, array( 'html', 'xhtml' ) ) ) {
					return false;
				}
				break;

			case 'class':
			case 'id':
			case 'is_array':
				if ( ! $this->_check_valid_attr( $val ) ) {
					return false;
				}
				break;

			case 'novalidate':
			// case 'add_honeypot':
			case 'form_element':
			case 'add_submit':
			case 'wrap_input':
				if ( ! is_bool( $val ) ) {
					return false;
				}
				break;

			case 'text_submit':
				if ( ! $this->_check_valid_attr( $val ) ) {
					return false;
				}
				break;

			case 'add_nonce':
				if ( ! is_string( $val ) && ! is_bool( $val ) ) {
					return false;
				}
				break;

			default:
				return false;

		endswitch;

		$this->form[ $key ] = $val;

		return true;

	}

	/**
	 * Add an input field to the form for outputting later
	 *
	 * @param string $label
	 * @param string $args
	 * @param string $slug
	 */
	function add_input( $label, $args = '', $slug = '' ) {

		if ( empty( $args ) ) {
			$args = array();
		}

		// Create a valid id or class attribute
		if ( empty( $slug ) ) {
			$slug = $this->_make_slug( $label );
		}

		// Tif - remove [] from id
		$id = str_replace( '[', '-', $slug );
		$id = str_replace( '_', '-', $id );
		$id = str_replace( ']', '', $id );

		$defaults = array(
			'type'				=> 'text',
			'name'				=> $slug,
			'id'				=> $id,
			'label'				=> $label,
			'value'				=> '',
			'placeholder'		=> '',
			'class'				=> array(),
			'min'				=> '',
			'max'				=> '',
			'step'				=> '',
			'autofocus'			=> false,
			'checked'			=> false,
			'selected'			=> false,
			'required'			=> false,
			'add_label'			=> true,
			'options'			=> array(),
			'wrap_tag'			=> 'div',
			'wrap_class'		=> array( 'wrap-option' ),
			'wrap_id'			=> $id . '-wrap',
			'wrap_style'		=> '',
			'before_html'		=> '',
			'after_html'		=> '',
			'request_populate'	=> true,

			'lang'				=> false,
			'error'				=> false,
			'rows'				=> false,
			'is_admin'			=> false,
			'header'			=> false,
			// 'wrap_input'		=> false,
			'is_sortable'		=> false,
			'is_deactivable'	=> false,
			'is_img'			=> false,
			'array'				=> array(),
			'is_closed'			=> false,
			'close'				=> false,
			'first'				=> false,
			'dashicon'			=> '',
			'colorpicker'		=> false,
			'default'			=> '',
			'mediauploader'		=> false,
			'imageid'			=> false,
			'description'		=> '',
			'description_top'	=> false,
			'info'				=> '',
		);

		// Combined defaults and arguments
		// Arguments override defaults
		$args					= array_merge( $defaults, $args );
		$this->inputs[ $slug ]	= $args;

	}

	/**
	 * Add multiple inputs to the input queue
	 *
	 * @param $arr
	 *
	 * @return bool
	 */
	function add_inputs( $arr ) {

		if ( ! is_array( $arr ) ) {
			return false;
		}

		foreach ( $arr as $field ) {
			$this->add_input(
				$field[0], isset( $field[1] ) ? $field[1] : '',
				isset( $field[2] ) ? $field[2] : ''
			);
		}

		return true;
	}

	/**
	 * Build the HTML for the form based on the input queue
	 *
	 * @param bool $echo Should the HTML be echoed or returned?
	 *
	 * @return string
	 */
	function build_form( $echo = true ) {

		$output = '';

		if ( $this->form['form_element'] ) {

			$output .= '<form method="' . $this->form['method'] . '"';

			if ( ! empty( $this->form['enctype'] ) ) {
				$output .= ' enctype="' . $this->form['enctype'] . '"';
			}

			if ( ! empty( $this->form['action'] ) ) {
				$output .= ' action="' . $this->form['action'] . '"';
			}

			if ( ! empty( $this->form['id'] ) ) {
				$output .= ' id="' . $this->form['id'] . '"';
			}

			if ( count( $this->form['class'] ) > 0 ) {
				$output .= $this->_output_classes( $this->form['class'] );
			}

			if ( $this->form['novalidate'] ) {
				$output .= ' novalidate';
			}

			$output .= '>';

		}

		// Add honeypot anti-spam field
		// if ( $this->form['add_honeypot'] ) {
		// 	$this->add_input( 'Phone', array(
		// 		'name'				=> 'tif_phone',
		// 		'slug'				=> 'tif_phone',
		// 		'id'				=> 'tif_phone',
		// 		'wrap_tag'			=> 'div',
		// 		'wrap_class'		=> array( 'wrap-option', '' ),
		// 		'wrap_id'			=> '',
		// 		// 'wrap_style'		=> 'display: none',
		// 		'request_populate'	=> false
		// 	) );
		// }

		// Add a WordPress nonce field
		if ( $this->form['add_nonce'] && function_exists( 'wp_create_nonce' ) ) {
			$this->add_input( 'WordPress nonce', array(
				'value'				=> wp_create_nonce( $this->form['add_nonce'] ),
				'add_label'			=> false,
				'type'				=> 'hidden',
				'request_populate'	=> false
			),
			'_wpnonce'
		);
		}

		// Iterate through the input queue and add input HTML
		foreach ( $this->inputs as $val ) :

			// register_setting("tif_myplugin", $val['name']);
			$min_max_range = $element = $end = $attr = $field = $label_html = '';

			// Automatic population of values using $_REQUEST data
			if ( $val['request_populate'] && isset( $_REQUEST[ $val['name'] ] ) ) {

				// Can this field be populated directly?
				if ( ! in_array( $val['type'], array( 'html', 'title', 'radio', 'checkbox', 'select', 'submit' ) ) ) {
					$val['value'] = $_REQUEST[ $val['name'] ];
				}
			}

			// Automatic population for checkboxes and radios
			if (
				$val['request_populate'] &&
				( $val['type'] == 'radio' || $val['type'] == 'checkbox' ) &&
				empty( $val['options'] )
			) {
				$val['checked'] = isset( $_REQUEST[ $val['name'] ] ) ? true : $val['checked'];
			}

			$help = ! empty( $val['info']) ? '<a href="#" class="info"  onclick="return false"><span>' . $val['info'] . '</span></a>' : '' ;
			$error = ! empty( $val['error'] ) ? '<small class="error-message">' . $val['error'] . '</small>'."\n" : '' ;
			$description = ! empty( $val['description'] ) ? '<small>' . $val['description'] . '</small>'."\n" : '' ;
			$description_top = $val['description_top'] ? $description : null ;
			$description_bottom = ! $val['description_top'] ? $description : null ;
			// $wrap_input_class = $val['type'] == 'radio' && $val['is_img'] ? ' tif-radio-img' : '' ;
			$lang = $val['lang'] ? ' lang="' . $val['lang'] . '"' : '' ;
			$disabled = $val['is_admin'] && ! current_user_can( 'manage_options' ) ? ' disabled' : '' ;
			$ul_class = $multicheck_ul_open = $multicheck_ul_close = $multicheck_li_open = $multicheck_li_close = '' ;

			$placeholder = $val['placeholder'] ? ' placeholder="' . esc_html($val['placeholder']) . '"' : '' ;

			if ( count( $val['options'] ) > 0 ) :

				$ul_is_sortable = $val['type'] == 'checkbox' && $val['is_sortable'] || $val['type'] == 'array' ? 'tif-sortable' : '' ;

				$ul_class = $val['type'] == 'checkbox' && $val['is_sortable'] ? ' tif-multicheck-sortable' : 'tif-multicheck' ;
				$ul_class = $val['type'] == 'array' ? ' tif-array' : $ul_class ;

				if ($val['type'] == 'radio')
					$ul_class = $val['is_img'] ? 'tif-radio-img' : 'tif-radio' ;

				$multicheck_ul_open = '<ul class="' . $ul_is_sortable  . $ul_class . '">' ;
				$multicheck_checkall = $ul_class == "tif-multicheck" ? '<input type="checkbox" name="checkall" class="checkall" /><i>' . esc_html__( 'Check all', 'canopee' ) . '</i><hr>': null ;
				$multicheck_ul_close = '</ul>' ;
				$multicheck_li_open = '<li class="' . $ul_class . '-item">' ;
				$multicheck_li_close = '</li>' ;
			endif;

			$wrap_input_open = $wrap_input_close = '';
			if ( $this->form['wrap_input'] ) :
				$wrap_input_open = '<div class="wrap-input">';
				$wrap_input_close = '</div><!-- .wrap-input -->';
			endif;

			switch ( $val['type'] ) {

				case 'html':
					$element = '';
					$end = $val['value'];
					break;

				case 'content':
					$element = '';
					$end = '';

					$end .= $val['value'];

					$end .= $error . $description_bottom ;
					$end .= $wrap_input_close ;
					$end .= $help;
					$label_html = '<label>' . $val['label'] . '</label>' . $wrap_input_open  ;
					break;

				case 'tabs':
				case 'subtabs':
					$element = '';
					$tab_open = $val['checked'] == $val['value'] ? 'checked="checked"' : '';
					$tab_name = $this->form['is_array'] ? $this->form['is_array'].'[tif_tabs]' : 'tif-tabs' ;
					$tab_name = $val['type'] == 'subtabs' ? $this->form['is_array'].'[tif_subtabs]' : $tab_name ;
					$tab_dashicon = ! empty( $val['dashicon'] ) ? '<i class="dashicons ' . $val['dashicon'] . '"></i>' : null ;
					$end = ! $val['first'] ? '</div>' : '' ;
					$end .= "\n".'<input id="' . $val['id'] . '" type="radio" name="' . $tab_name .'" value="' . $val['value'] . '" class="hidden" ' . $tab_open . ' />
					<label for="' . $val['id'] . '" id="' . $val['id'] . '-label" class="subtab">' . $tab_dashicon . $val['label'] . '</label>
					<div class="tab-content ' . $val['id'] . '">'."\n";
					break;

				case 'tags':
					$element = '' ;
					$end = '';
					break;

				case 'title':
					$element = '';
					$end     = '
					<h3>' . $val['label'] . '</h3>';
					break;

				case 'textarea':
					$element = 'textarea';
					$rows    =  (int)$val['rows'] ? ' rows="' . $val['rows'] . '"' : '' ;
					$end     = $lang . $disabled . $rows . '>' . $val['value'] . '</textarea>';
					break;

				case 'select':
					$element  = 'select';
					$end     .= $lang . $disabled . '>';
					foreach ( $val['options'] as $key => $opt ) {
						$opt_insert = '';
						if (
							// Is this field set to automatically populate?
							$val['request_populate'] &&

							// Do we have $_REQUEST data to use?
							isset( $_REQUEST[ $val['name'] ] ) &&

							// Are we currently outputting the selected value?
							$_REQUEST[ $val['name'] ] === $key
						) {
							$opt_insert = ' selected';

						// Does the field have a default selected value?
						} else if ( $val['selected'] === $key ) {
							$opt_insert = ' selected';
						}
						$end .= '<option value="' . $key . '"' . $opt_insert . '>' . $opt . '</option>';
					}
					$end .= '</select>';
					break;

				case 'radio':
				case 'checkbox':

					// Special case for multiple check boxes
					if ( count( $val['options'] ) > 0 ) :

						// Tif - multiple checkbox value
						if ( $val['type'] === 'checkbox' )
							/* Data */
							$multi_values = ! is_array( $val['value'] ) ? explode( ',', $val['value'] ) : $val['value'];

						$element = '';

						if ( $val['is_sortable'] ) {

							$choices = $val['options'];

							$values = array();
							foreach ( $multi_values as $key => $multi_values ) {
								$values[$key] = ( strpos( $multi_values, ':' ) == false ) ? $multi_values . ':1' : $multi_values ; /* activate all as default. */
							}

							/* If values exist, use it. */
							$options = array();
							if ( $values ){

								/* get individual item */
								foreach ( $values as $value ){

									/* separate item with option */
									$value = explode( ':', $value );

									/* build the array. remove options not listed on choices. */
									if ( array_key_exists( $value[0], $choices ) ){
										$options[$value[0]] = $value[1] ? '1' : '0';
										$hidden_input_value[] = $value[0] . ':' . $value[1];
									}
								}
							}

							/* if there's new options (not saved yet), add it in the end. */
							foreach ( $choices as $key => $value_ ){

								/* if not exist, add it in the end. */
								if ( ! array_key_exists( $key, $options ) ){
									$options[$key] = '0'; // use zero to deactivate as default for new items.
									$hidden_input_value[] = $key . ':0' ;
								}
							}
							$hidden_input_value = implode( ',', $hidden_input_value );

							foreach ( $options as $key => $opt ){

								$slug = $this->_make_slug( $opt );
								$end .= $multicheck_li_open ;

								if ( $val['is_sortable'] )
									$end .= '<i class="dashicons dashicons-sort"></i>';

								$end .= sprintf(
									'<input type="%s" name="%s" value="%s" id="%s"',
									$val['type'],
									$val['name'],
									$key,
									$slug
								);
								if (
									// Tif - checked if key
									// $val['checked'] == $key ||
									$options[$key] == 1 ||

									// Is this field set to automatically populate?
									$val['request_populate'] &&

									// Do we have $_REQUEST data to use?
									isset( $_REQUEST[ $val['name'] ] ) &&

									// Is the selected item(s) in the $_REQUEST data?
									in_array( $key, $_REQUEST[ $val['name'] ] )
								) {
									$end .= ' checked';
								}

								$end .= ' class="' . $ul_class . '-item-input"';

								$end .= $lang . $disabled;
								$end .= $this->field_close();

								$end .= ' <label for="' . $slug . '">';
								$end .= $choices[$key];
								$end .= '</label>';

								$end .= $multicheck_li_close ;

							} // end choices.

						} else {

							if ( count($val['options'] ) <= 1 )
								$multicheck_checkall = null;

							foreach ( $val['options'] as $key => $opt ) {

								$txt = null;
								if ( is_array( $opt ) ) :
									$txt = '<small>' . (string)$opt[1] . '</small>';
									$opt = $opt[0];
								endif;

								$slug = $this->_make_slug( $opt );
								$end .= $multicheck_li_open ;
								$end .= sprintf(
									'<input type="%s" name="%s" value="%s" id="%s"',
									$val['type'],
									$val['name'],
									$key,
									$slug
								);
								if (
									// Tif - checked if key
									$val['checked'] == $key ||
									$val['type'] === 'checkbox' && in_array( $key, $multi_values ) ||

									// Is this field set to automatically populate?
									$val['request_populate'] &&

									// Do we have $_REQUEST data to use?
									isset( $_REQUEST[ $val['name'] ] ) &&

									// Is the selected item(s) in the $_REQUEST data?
									in_array( $key, $_REQUEST[ $val['name'] ] )
								) {
									$end .= ' checked';
								}

								$end .= ' class="' . $ul_class . '-item-input"';
								$end .= $lang . $disabled;
								$end .= $this->field_close();

								$end .= ' <label for="' . $slug . '">';

								if ( $val['type'] === 'radio' && $val['is_img'] == true )
									$end .= '<img src="' . $opt . '" />' . $txt;
								else
									$end .= $opt;

								$end .= '</label>';
								$end .= $multicheck_li_close ;

								if ( $val['type'] === 'checkbox' )
									$hidden_input_value = implode( ',', $multi_values );

							}

						}

						// Tif - multiple checkbox value
							if ( $val['type'] === 'checkbox' )
								$end .= '<input ' . $lang . $disabled . ' id="' . $val['name'] . '" name="' . $val['name'] . '" type="hidden" class="' . $ul_class . '-input-hidden" value="' . esc_attr( $hidden_input_value ) . '">';

						$end .= $multicheck_ul_close ;
						$end .= $error . $description_bottom ;
						$end .= $wrap_input_close ;
						$end .= $help;
						$label_html = '<header>' . $val['label'] . '</header>' . $wrap_input_open . $description_top . $multicheck_ul_open . $multicheck_checkall;
						break;

					endif;

				case 'array':

					// Special case for sortable fields
					if ( count( $val['options'] ) > 0 ) :

						$filter_sortable_order = ! empty( $val['value']) ? $val['value'] : $val['options'] ;

						foreach ( array_slice( $filter_sortable_order, 0 ) as $key => $opt ):
							$name = isset( $opt['name'] ) ? $opt['name'] : $key ;

							$end .= $multicheck_li_open ;
							$end .= '<i class="dashicons dashicons-sort"></i>';
							$end .= '<input ' . $lang . $disabled . ' type="hidden" name="' . $val['name'] . '[' . $key . ']" value="1" />';

							$i = 0;
							if ( is_array( $opt ) ) foreach ( $opt as $k => $v ):

								// if ($i == 0 && $val['is_deactivable']) :
								if ( $i == 0 ) :

									if ( $val['is_deactivable'] ) :
										$end .= '<input type="checkbox" name="' . $val['name'] . '[' . $key . '][enabled]" value="1"';

										if (
											// Tif - checked if key
											( isset( $opt['enabled'] ) && $opt['enabled'] ) ||

											// Is this field set to automatically populate?
											$val['request_populate'] &&

											// Do we have $_REQUEST data to use?
											isset( $_REQUEST[ $val['name'] ] ) &&

											// Is the selected item(s) in the $_REQUEST data?
											in_array( $key, $_REQUEST[ $val['name'] ] )
										) {
											$end .= ' checked';
										}
										$end .= $lang . $disabled;
										$end .= $this->field_close();

									endif;

								endif;

								if ( $k != 'enabled' )
									$end .= '<input ' . $lang . $disabled . ' type="hidden" name="' . $val['name'] . '[' . $key . '][' . $k . ']" value="' . $v . '" />';
							++$i;
							endforeach;

							$end .= ' <label for="' . $key . '">' . $name . '</label>';
							$end .= $multicheck_li_close ;

						endforeach;

						$end .= $multicheck_ul_close ;
						$end .= $error . $description_bottom ;
						$end .= $wrap_input_close ;
						$end .= $help;
						$label_html = '<header>' . $val['label'] . '</header>' . $wrap_input_open . $multicheck_ul_open ;


						break;
					endif;

				// Used for all text fields (text, email, url, etc), single radios, single checkboxes, and submit
				default :
					$element = 'input';
					$end .= ' type="' . $val['type'] . '" value="' . $val['value'] . '"';
					$end .= $val['colorpicker'] && $val['default'] ? ' data-default-color="' .$val['default'] . '"' : ''; // used pour default color picker
					$end .= $val['checked'] ? ' checked' : '';
					$end .= $placeholder;
					$end .= $lang . $disabled;
					$end .= $this->field_close( true );

					if ( $val['mediauploader'] ) :

						$src = $val['value'];

						if ( is_numeric( $val['value'] ) )
							$src = wp_get_attachment_image_url( (int)$val['value'] );

						$hidden = empty( $val['value'] ) ? ' hidden' : '' ;

						$end .= '<img src="' . esc_url_raw( $src ) . '" class="image-preview' . $hidden . '">';
						$end .= '<input class="btn-upload-image button button-primary" type="button" value="Add image">&nbsp;';
						$end .= '<input class="btn-remove-image button button-error' . $hidden . '" type="button" value="Remove Image" />' ;
					endif;

					break;

			}

			// Added a submit button, no need to auto-add one
			if ( $val['type'] === 'submit' ) {
				$this->has_submit = true;
			}

			// Special number values for range and number types
			if ( $val['type'] === 'range' || $val['type'] === 'number'  || $val['type'] === 'date' ) {
				$min_max_range .= ! empty( $val['min'] ) ? ' min="' . $val['min'] . '"' : '';
				$min_max_range .= ! empty( $val['max'] ) ? ' max="' . $val['max'] . '"' : '';
				$min_max_range .= ! empty( $val['step'] ) ? ' step="' . $val['step'] . '"' : '';
			}

			// Add an ID field, if one is present
			$id = ! empty( $val['id'] ) ? ' id="' . $val['id'] . '"' : '';

			// Tif - Add class image-url if $val['mediauploader'] == true
			$_class_mediauploader = $val['imageid'] ? array( 'image-id') :  array( 'image-url') ;
			$_class = $val['mediauploader'] ? array_merge( $_class_mediauploader, $val['class'] )  : $val['class'] ;

			// Add error class
			$_class = ! empty( $val['error'] ) ? array_merge( $_class, array( 'error' ) ) : $_class ;
			$_class = ! empty( $val['required'] ) ? array_merge( $_class, array( 'required-field' ) ) : $_class ;
			$_class = $val['colorpicker'] ? array_merge( $_class, array( 'color' ) ) : $_class ;

			// Output classes
			$class = $this->_output_classes( $_class );

			// Special HTML5 fields, if set
			$attr .= $val['autofocus'] ? ' autofocus' : '';
			$attr .= $val['checked'] ? ' checked' : '';
			$attr .= $val['required'] ? ' required' : '';

			// Build the label
			if ( $val['type'] == 'tags' ) {

					$class = ! $val['close'] && ! empty( $val['class']) ? $class : '' ;
					$open = ! $val['close']  ? '<' . $val['wrap_tag'] . ' id="' . $val['id'] . '" name="' . $val['name'] . '"' . $class . '>' : '' ;
					$end = $val['close'] || $val['is_closed'] ? '</' . $val['wrap_tag'] . '>' : '' ;
					$label = $val['wrap_tag'] == 'fieldset' && ! $val['close'] ? '<legend>' . $val['label'] . '</legend>' : $val['label'] ;

					$output .= $open . $label . $end;

			} else {
				$label = null;
				if ( ! empty( $label_html ) ) {
					$field .= $label_html;
				} elseif ( $val['add_label'] && ! in_array( $val['type'], array( 'hidden', 'submit', 'title', 'html', 'tabs', 'subtabs' ) ) ) {

					if ( $val['required'] )
						$val['label'] .= ' <strong class="tif-required">*</strong>';

					if ( $val['type'] === 'checkbox' && $this->form['wrap_input'] ) {
						$field .= '<label for="' . $val['id'] . '">' . $val['label'] . '</label>'."\n\n";
						$label = null;
					} else {
						$field .= null;
						$label = '<label for="' . $val['id'] . '">' . $val['label'] . '</label>'."\n\n";
					}
				}

				// An $element was set in the $val['type'] switch statement above so use that
				if ( ! empty( $element ) ) {
					if ( $val['type'] === 'checkbox' || ! $this->form['wrap_input'] ) {
						$field = '
						' . $wrap_input_open . '<' . $element . $id . ' name="' . $val['name'] . '"' . $min_max_range . $class . $attr . $end . $label . $error . $description_bottom . $wrap_input_close . $help . $field;
					} else {
						$field .= $label .'
						' . $wrap_input_open . '<' . $element . $id . ' name="' . $val['name'] . '"' . $min_max_range . $class . $attr . $end  . $error . $description_bottom . $wrap_input_close . $help ;
					}
				// Not a form element
				} else {
					$field .= $end;
				}

				// Parse and create wrap, if needed
				if ( $val['type'] != 'hidden' && $val['type'] != 'html' && $val['type'] != 'tabs' && $val['type'] != 'subtabs' ) :

					$wrap_before = $val['before_html'];
					if ( ! empty( $val['wrap_tag'] ) ) {
						$wrap_before .= '<' . $val['wrap_tag'];
						$wrap_before .= count( $val['wrap_class'] ) > 0 ? $this->_output_classes( $val['wrap_class'] ) : '';
						$wrap_before .= ! empty( $val['wrap_style'] ) ? ' style="' . $val['wrap_style'] . '"' : '';
						$wrap_before .= ! empty( $val['wrap_id'] ) ? ' id="' . $val['wrap_id'] . '"' : '';
						$wrap_before .= '>';
					}

					$wrap_after = $val['after_html'];
					if ( ! empty( $val['wrap_tag'] ) )
						$wrap_after = '</' . $val['wrap_tag'] . '>' . $wrap_after;

					$output .= $wrap_before . $field . $wrap_after;
				else :
					$output .= $field;
				endif;

			}

		endforeach;

		// Auto-add submit button
		if ( ! $this->has_submit && $this->form['add_submit'] ) {

			$output .= '<div class="form-submit"><input type="submit" id="submit" value="' . $this->form['text_submit'] . '" name="submit" class="';
			$output .= is_admin() ? 'button button--primary' : 'btn btn--primary' ;
			$output .= '"></div>';

		}

		// Close the form tag if one was added
		if ( $this->form['form_element'] ) {
			$output .= '</form>';
		}

		// Output or return?
		if ( $echo ) {
			echo $output;
		} else {
			return $output;
		}
	}

	// Easy way to auto-close fields, if necessary
	function field_close( $control = false ) {

		$field_close = $this->form['markup'] === 'xhtml' ? ' />' : '>';
		$input_control = $control ? '<span class="input-control"></span>' : '' ;
		return $field_close . $input_control;

	}

	// Validates id and class attributes
	// TODO: actually validate these things
	private function _check_valid_attr( $string ) {

		$result = true;

		// Check $name for correct characters
		// "^[a-zA-Z0-9_-]*$"

		return $result;

	}

	// Create a slug from a label name
	private function _make_slug( $string ) {

		$result = '';

		$result = str_replace( '"', '', $string );
		$result = str_replace( "'", '', $result );
		$result = str_replace( '_', '-', $result );
		$result = preg_replace( '~[\W\s]~', '-', $result );

		$result = strtolower( $result );

		return $result;

	}

	// Parses and builds the classes in multiple places
	private function _output_classes( $classes ) {

		$output = '';


		if ( is_array( $classes ) && count( $classes ) > 0 ) {
			$output .= ' class="';
			foreach ( $classes as $class ) {
				$output .= $class . ' ';
			}
			$output .= '"';
		} else if ( is_string( $classes ) ) {
			$output .= ' class="' . $classes . '"';
		}

		return $output;
	}

}
