<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 */
if ( ! function_exists( 'tif_loop_layout_thumb_size_control' ) ) {

	add_action( 'customize_register', 'tif_loop_layout_thumb_size_control' );

	function tif_loop_layout_thumb_size_control( $wp_customize ) {

		if ( ! class_exists( 'WP_Customize_Control' ) )
		return null;

		class Tif_Customize_Loop_Layout_Thumb_Size_Control extends WP_Customize_Control {

			public function render_content() {

				if ( empty( $this->choices ) )
					return;

				$name = '_customize-radio-' . $this->id;

				if ( ! empty( $this->label ) ) // add label if needed.
					echo '<span class="customize-control-title">' . esc_html( $this->label ) . '</span>';

				if ( ! empty( $this->description ) ) // add desc if needed.
					echo '<span class="description customize-control-description">' .  wp_kses( $this->description, tif_allowed_html() ) . '</span>';

				?>

				<ul class="tif-loop-layout-control loop-settings-posts-list-layout-control tif-radio-img">

				<?php

				$this_value  = $this->value();
				$this_value  = tif_sanitize_loop_layout( $this_value );

				foreach ( $this->choices as $value => $label ) :

					$src   = is_array( $label ) ? $label[0] : $label;
					$legend = is_array( $label ) ? (string)$label[1] : null;

					?>

					<li class="tif-radio-img-item">

						<input
						id="<?php echo esc_attr( $name ) . '_' . esc_attr( $value ); ?>"
						type="radio" value="<?php echo esc_attr( $value ); ?>"
						name="<?php echo esc_attr( $name ); ?>"
						class="tif-radio-img-item-input"

						<?php
						checked( $this_value[0], $value );
						?>
						/>

						<label for="<?php echo esc_attr( $name ) . '_' . esc_attr( $value ); ?>">
							<img src="<?php echo esc_url( $src ); ?>" />
							<?php if ( null != $legend ) echo '<small>' . esc_attr( $legend ) . '</small>' ?>
						</label>

					</li>

					<?php

				endforeach;

				?>

				<li>
					<label>
						<?php _e( 'How many items per line?', 'canopee' ) ?>
						<input type="number" value="<?php echo (int)$this_value[1]; ?>" min="1" max="4" />
					</label>
				</li>

				<li>
					<label>
						<?php _e( 'Thumbnail size', 'canopee' ) ?>
						<select>
							<option value="small" <?php selected( $this_value[2], 'small' ); ?>><?php _e( 'Small', 'canopee' ) ?></option>
							<option value="medium" <?php selected( $this_value[2], 'medium' ); ?>><?php _e( 'Medium', 'canopee' ) ?></option>
							<option value="large" <?php selected( $this_value[2], 'large' ); ?>><?php _e( 'Large', 'canopee' ) ?></option>
						</select>
					</label>
				</li>

				<li>
					<label>
						<?php _e( 'Container additional CSS Class(es)', 'canopee' ) ?>
						<input class="container-class" type="text" value="<?php echo esc_attr( $this_value[3] ); ?>" />
					</label>
				</li>

				<li>
					<label>
						<?php _e( 'Post additional CSS Class(es)', 'canopee' ) ?>
						<input class="post-class" type="text" value="<?php echo esc_attr( $this_value[4] ); ?>" />
					</label>
				</li>

				<input type="hidden" <?php $this->link(); ?> value="<?php echo esc_attr( $this->value() ); ?>" />
				</ul>

				<?php
			}

		}

	}

}
