<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 */
if ( ! function_exists( 'tif_loop_layout_thumb_ratio_control' ) ) {

	add_action( 'customize_register', 'tif_loop_layout_thumb_ratio_control' );

	function tif_loop_layout_thumb_ratio_control( $wp_customize ) {

		if ( ! class_exists( 'WP_Customize_Control' ) )
		return null;

		class Tif_Customize_Loop_Layout_Thumb_Ratio_Control extends WP_Customize_Control {
			public function render_content() {

				if ( empty( $this->choices ) )
					return;

				$name = '_customize-radio-' . $this->id;

				if ( ! empty( $this->label ) ) // add label if needed.
					echo '<span class="customize-control-title">' . esc_html( $this->label ) . '</span>';

				if ( ! empty( $this->description ) ) // add desc if needed.
					echo '<span class="description customize-control-description">' .  wp_kses( $this->description, tif_allowed_html() ) . '</span>';

				?>

				<ul class="tif-loop-layout-control tif-radio-img">

				<?php

				$this_value  = $this->value();
				$this_value  = tif_sanitize_loop_layout( $this_value );

				foreach ( $this->choices as $value => $label ) :

					$src   = is_array( $label ) ? $label[0] : $label;
					$legend = is_array( $label ) ? (string)$label[1] : null;

					?>

					<li class="tif-radio-img-item">

						<input
						id="<?php echo esc_attr( $name ) . '_' . esc_attr( $value ); ?>"
						type="radio" value="<?php echo esc_attr( $value ); ?>"
						name="<?php echo esc_attr( $name ); ?>"
						class="tif-radio-img-item-input"

						<?php
						checked( $this_value[0], $value );
						?>
						/>

						<label for="<?php echo esc_attr( $name ) . '_' . esc_attr( $value ); ?>">
							<img src="<?php echo esc_url( $src ); ?>" />
							<?php if ( null != $legend ) echo '<small>' . esc_attr( $legend ) . '</small>' ?>
						</label>

					</li>

					<?php

				endforeach;

				?>

				<li>
					<label>
						<?php _e( 'How many items per line?', 'canopee' ) ?>
						<input type="number" value="<?php echo (int)$this_value[1]; ?>" min="1" max="4" />
					</label>
				</li>

				<li>
					<label>
						<?php _e( 'Thumbnail ratio', 'canopee' ) ?>
						<select>
						    <option value="1.1" <?php  selected( $this_value[2], '1.1' ); ?>><?php  _e( '1:1', 'canopee' ) ?></option>
						    <option value="6.5" <?php  selected( $this_value[2], '6.5' ); ?>><?php  _e( '6:5', 'canopee' ) ?></option>
						    <option value="4.3" <?php  selected( $this_value[2], '4.3' ); ?>><?php  _e( '4:3', 'canopee' ) ?></option>
						    <option value="3.2" <?php  selected( $this_value[2], '3.2' ); ?>><?php  _e( '3:2', 'canopee' ) ?></option>
						    <option value="5.3" <?php  selected( $this_value[2], '5.3' ); ?>><?php  _e( '5:3', 'canopee' ) ?></option>
						    <option value="16.9" <?php selected( $this_value[2], '16.9' ); ?>><?php _e( '16:9', 'canopee' ) ?></option>
						    <option value="2.1" <?php  selected( $this_value[2], '2.1' ); ?>><?php  _e( '2:1', 'canopee' ) ?></option>
						    <option value="3.1" <?php  selected( $this_value[2], '3.1' ); ?>><?php  _e( '3:1', 'canopee' ) ?></option>
						    <option value="4.1" <?php  selected( $this_value[2], '4.1' ); ?>><?php  _e( '4:1', 'canopee' ) ?></option>
						</select>
					</label>
				</li>

				<li>
					<label>
						<?php _e( 'Container additional CSS Class(es)', 'canopee' ) ?>
						<input class="container-class" type="text" value="<?php echo esc_attr( $this_value[3] ); ?>" />
					</label>
				</li>

				<li>
					<label>
						<?php _e( 'Post additional CSS Class(es)', 'canopee' ) ?>
						<input class="post-class" type="text" value="<?php echo esc_attr( $this_value[4] ); ?>" />
					</label>
				</li>

				<input type="hidden" <?php $this->link(); ?> value="<?php echo esc_attr( $this->value() ); ?>" />
				</ul>

				<?php
			}

		}

	}

}
