<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 */
if ( ! function_exists( 'tif_extend_select_multiple_control' ) ) {

	add_action( 'customize_register', 'tif_extend_select_multiple_control' );

	function tif_extend_select_multiple_control( $wp_customize ) {

		if ( ! class_exists( 'WP_Customize_Control' ) )
		return null;

		class Tif_Customize_Select_Multiple_Control extends WP_Customize_Control {

			/**
			 * Multiple checkbox customize control class.
			 *
			 * @since  1.0.0
			 * @access public
			 */

			public $type = 'select-multiple';

			public function render_content() {

				if ( empty( $this->choices ) )
					return;

				if ( ! empty( $this->label ) ) // add label if needed.
					echo '<span class="customize-control-title">' . esc_html( $this->label ) . '</span>';

				if ( ! empty( $this->description ) ) // add desc if needed.
					echo '<span class="description customize-control-description">' .  wp_kses( $this->description, tif_allowed_html() ) . '</span>';

				$multi_values = ! is_array( $this->value() ) ? explode( ',', $this->value() ) : $this->value();

				?>


				<ul class="tif-multiselect">

					<?php

					$i = 0;
					foreach ( $this->choices as $value => $label ) :

					?>

						<li class="tif-multiselect-item">
							<label>

								<?php echo esc_html( $label ); ?>

								<select class="tif-multiselect-select" name="<?php esc_attr( $value ) ?>">

									<?php

									foreach ( $this->input_attrs as $key => $option ) {
										echo '<option value="' . tif_sanitize_key( $key ) . '"' . ( $key == $multi_values[$i] ? ' selected' : null ) . '>' . esc_html( $option ) . '</option>';
									}

									?>
								</select>
							</label>
						</li>

					<?php

					++$i;
					endforeach;

					?>

					<input class="tif-multiselect-item-input" type="hidden" <?php $this->link(); ?> value="<?php echo esc_attr( implode( ',', $multi_values ) ); ?>" />

				</ul>

			<?php
			}
		}

	}

}
