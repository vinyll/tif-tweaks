<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 */
if ( ! function_exists( 'tif_extend_range_multiple_control' ) ) {

	add_action( 'customize_register', 'tif_extend_range_multiple_control' );

	function tif_extend_range_multiple_control( $wp_customize ) {

		if ( ! class_exists( 'WP_Customize_Control' ) )
		return null;

		if ( ! class_exists ( 'Tif_Customize_Range_Multiple_Control' ) ) {

			class Tif_Customize_Range_Multiple_Control extends WP_Customize_Control {

				public $type = 'number-multiple';

				public function render_content() {

					if ( empty( $this->choices ) )
						return;

					if ( ! empty( $this->label ) ) // add label if needed.
						echo '<span class="customize-control-title">' . esc_html( $this->label ) . '</span>';

					if ( ! empty( $this->description ) ) // add desc if needed.
						echo '<span class="description customize-control-description">' .  wp_kses( $this->description, tif_allowed_html() ) . '</span>';

					$multi_values = !is_array( $this->value() ) ? explode( ',', $this->value() ) : $this->value();
					$alignment = isset( $this->input_attrs['alignment'] ) ? $this->input_attrs['alignment'] : 'column' ;
					$has_unit = isset( $this->choices['unit'] ) ? ' has-unit' : null ;

					?>

					<ul class="tif-multirange  <?php echo esc_attr( $alignment . $has_unit ) ?>">

						<?php

							$i = 0;
							foreach ( $this->choices as $value => $label ) :

							?>

									<li class="tif-multirange-item ">
										<label>
										<div>
											<?php

											echo esc_html( $label );

											if ( $value != 'unit' ) {

											?>

											<input class="tif-multirange-item-input" type="range" value="<?php echo (float)$multi_values[$i]; ?>"
											<?php
											echo ( isset( $this->input_attrs['min'] ) ? ' min="' . (float)$this->input_attrs['min'] . '"' : null );
											echo ( isset( $this->input_attrs['max'] ) ? ' max="' . (float)$this->input_attrs['max'] . '"' : null );
											echo ( isset( $this->input_attrs['step'] ) ? ' step="' . (float)$this->input_attrs['step'] . '"' : null );
											?>
											/>

											<?php

											} elseif ( $value == 'unit' ) {

												?>

												<select class="tif-length-select">
													<?php

													foreach ( $this->input_attrs['unit'] as $val=> $option ) :
														echo '<option value="' . esc_html( $val ) . '"' . ( $multi_values[$i] == $val ? ' selected' : null ) . '>' .  esc_html( $option ) . '</option>';
													endforeach;

													?>
												</select>

												<?php

											}

											?>

										</div>
										</label>
									</li>

							<?php


							++$i;
							endforeach;

							?>

					<input type="hidden" <?php $this->link(); ?> value="<?php echo esc_attr( implode( ',', $multi_values ) ); ?>" />

					</ul>

				<?php

				}

			}

		} // end if class_exists

	}

}
