<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 */
if ( ! function_exists( 'tif_customizer_alert' ) ) {

	add_action( 'customize_register', 'tif_customizer_alert' );

	function tif_customizer_alert( $wp_customize ) {

		if ( ! class_exists( 'WP_Customize_Control' ) )
			return null;

			class Tif_Customize_Alert_Control extends WP_Customize_Control {

				public function render_content() {

					if ( ! empty( $this->label ) )
						echo '<label class="customize-control-title"><span>' . esc_html( $this->label ) . '</span></label>';

					if ( ! empty( $this->description ) )
						echo '<div class="description customize-control-description' . ( isset( $this->input_attrs['alert'] ) ? ' tif-customizer-' . esc_attr( $this->input_attrs['alert'] ) : null ) . '">' .  $this->description . '</div>';


				}

			}

	}

}
