<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 */
if ( ! function_exists( 'tif_box_shadow_control' ) ) {

	add_action( 'customize_register', 'tif_box_shadow_control' );

	function tif_box_shadow_control( $wp_customize ) {

		if ( ! class_exists( 'WP_Customize_Control' ) )
			return null;

		class Tif_Customize_Box_Shadow_Control extends WP_Customize_Control {

			public function render_content() {

				$name = '_customize-radio-' . $this->id;

				if ( ! empty( $this->label ) ) // add label if needed.
					echo '<label class="tif-customizer-title heading sub-title"><span>' . esc_html( $this->label ) . '</span></label>';

				if ( ! empty( $this->description ) ) // add desc if needed.
					echo '<span class="description customize-control-description">' .  wp_kses( $this->description, tif_allowed_html() ) . '</span>';

				$multi_values = tif_sanitize_array_boxshadow( $this->value() );
				$format		  = isset( $this->input_attrs['format'] ) ? tif_sanitize_key( $this->input_attrs['format'] ) : 'hex' ;
				$name		  = '_customize-' . esc_attr( $format ) . '-box-shadow-' . $this->id;

				if ( isset( $this->input_attrs['tif'] ) && $this->input_attrs['tif'] == 'key' && class_exists ( 'Themes_In_France' ) ) {
					$format = 'key';
					$this->choices = tif_get_theme_key_colors();
				}
				?>

				<ul class="tif-key-array-color tif-key-box-shadow">

				<?php

				if ( $format == 'key' ) {

					if ( empty( $this->choices ) )
						return;

					foreach ( $this->choices as $value => $label ) :

						?>

						<li class="tif-key-box-shadow-item">

							<input
							id="<?php echo esc_attr( $name ) . '_' . esc_attr( $value ); ?>"
							type="radio" value="<?php echo esc_attr( $value ); ?>"
							name="<?php echo esc_attr( $name ); ?>"
							class="tif-key-input-colors"

							<?php
							$this->link();
							checked( $value, (string)$multi_values[4] );
							?>
							/>

							<label for="<?php echo esc_attr( $name ) . '_' . esc_attr( $value ); ?>">
								<img src="<?php echo TIF_ADMIN_IMAGES_URL . '/blank.png' ?>" style="background:<?php echo $label['color'] ?>;" class="tif-<?php echo tif_esc_css( $value ) ?>-background-color" />
							</label>

						</li>

						<?php

					endforeach;

				} else {

					?>

					<input type="text"
						id="<?php echo esc_attr( $name ) . '_' . esc_attr( $value ); ?>"
						class="tif-jscolor-input-text"
						value="<?php echo (string)$multi_values[4] ?>"
						data-jscolor="{
							format: '<?php echo esc_attr( $format ); ?>',
							value: '<?php echo (string)$multi_values[4] ?>',
							palette: '<?php echo implode( ',', $this->choices ) ?>'
						}"
					/>
					<script>
						jscolor.install();
					</script>
					<?php

				}

				?>

				<label class="tif-key-array-color-range">
					<span><?php echo esc_html__( 'Offset X', 'canopee' ) ?></span>
					<input type="range"
					class="tif-position-x-input-range"
					value="<?php echo (float)$multi_values[0]; ?>"
					min="-50"
					max="50"
					step=".1"
					/>
				</label>

				<label class="tif-key-array-color-range">
					<span><?php echo esc_html__( 'Offset Y', 'canopee' ) ?></span>
					<input type="range"
					class="tif-position-y-input-range"
					value="<?php echo (float)$multi_values[1]; ?>"
					min="-50"
					max="50"
					step=".1"
					/>
				</label>

				<label class="tif-key-array-color-range">
					<span><?php echo esc_html__( 'Blur', 'canopee' ) ?></span>
					<input type="range"
					class="tif-blur-input-range"
					value="<?php echo (float)$multi_values[2]; ?>"
					min="-50"
					max="50"
					step=".1"
					/>
				</label>

				<label class="tif-key-array-color-range">
					<span><?php echo esc_html__( 'Spread', 'canopee' ) ?></span>
					<input type="range"
					class="tif-spread-input-range"
					value="<?php echo (float)$multi_values[3]; ?>"
					min="-50"
					max="50"
					step=".1"
					/>
				</label>

				<label class="tif-key-array-color-range">
					<span><?php echo esc_html__( 'Opacity', 'canopee' ) ?></span>
					<input type="range"
					class="tif-opacity-input-range"
					value="<?php echo (float)$multi_values[5]; ?>"
					min="0"
					max="1"
					step=".05"
					/>
				</label>

				<label class="tif-key-array-color-range">
					<span><?php echo esc_html__( 'Inset', 'canopee' ) ?></span>
					<input type="checkbox"
					class=""
					value="inset"
					<?php checked( 'inset', $multi_values[6] ) ?>
					/>
				</label>

				<input type="hidden" <?php $this->link(); ?> value="<?php echo esc_attr( implode( ',', $multi_values ) ); ?>" />

				</ul>

				<?php

			}

			/**
			 * Enqueue our scripts and styles
			 */
			public function enqueue() {

				if ( $this->input_attrs['format'] != 'key' ) {

					wp_enqueue_style( 'tif-jscolor', Tif_Init::tif_get_tif_url() . 'assets/css/tif-jscolor.min.css', false, '1.0', false );
					wp_enqueue_script( 'tif-jscolor', Tif_Init::tif_get_tif_url() . 'assets/js/tif-jscolor.min.js', false, '1.0', false );

				}

			}

		}

	}

}
