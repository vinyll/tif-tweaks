<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 */
if ( ! function_exists( 'tif_extend_checkbox_sortable_control' ) ) {

	add_action( 'customize_register', 'tif_extend_checkbox_sortable_control' );

	function tif_extend_checkbox_sortable_control( $wp_customize ) {

		if ( ! class_exists( 'WP_Customize_Control' ) )
			return null;

		class Tif_Customize_Checkbox_Sortable_Control extends WP_Customize_Control {

			/**
			 * Control Type
			 */
			public $type = 'tif-multicheck-sortable';

			/**
			 * Render Settings
			 */
			public function render_content() {

				/* if no choices, bail. */
				if ( empty( $this->choices ) )
					return;

				if ( ! empty( $this->label ) ) // add label if needed.
					echo '<span class="customize-control-title">' . esc_html( $this->label ) . '</span>';

				if ( ! empty( $this->description ) ) // add desc if needed.
					echo '<span class="description customize-control-description">' .  wp_kses( $this->description, tif_allowed_html() ) . '</span>';

				/* Data */
				$multi_values = ! is_array( $this->value() ) ? explode( ',', $this->value() ) : $this->value();
				// $choices = array();

				$choices = $this->choices;

				$values = array();
				foreach ( $multi_values as $key => $multi_values ) {
					$values[$key] = ( strpos( $multi_values, ':' ) == false ) ? $multi_values . ':1' : $multi_values ; /* activate all as default. */
				}

				/* If values exist, use it. */
				$options = array();
				if ( $values ){

					/* get individual item */
					foreach ( $values as $value ){

						/* separate item with option */
						$value = explode( ':', $value );

						/* build the array. remove options not listed on choices. */
						if ( array_key_exists( $value[0], $choices ) ){
							$options[$value[0]] = $value[1] ? '1' : '0';
							$hidden_input_value[] = $value[0] . ':' . $value[1];
						}
					}
				}

				/* if there's new options (not saved yet), add it in the end. */
				foreach ( $choices as $key => $value_ ) {

					/* if not exist, add it in the end. */
					if ( ! array_key_exists( $key, $options ) ){
						$options[$key] = '0'; // use zero to deactivate as default for new items.
						$hidden_input_value[] = $key . ':0' ;
					}
				}

				$hidden_input_value = implode( ',', $hidden_input_value );

				?>

				<ul class="tif-sortable tif-multicheck-sortable">

					<?php foreach ( $options as $key => $opt ) { ?>

						<li class="tif-multicheck-sortable-item">
							<label>
								<input id="<?php echo $this->id . '_' . esc_attr( $key ); ?>" name="<?php echo esc_attr( $key ); ?>" class="tif-multicheck-sortable-item-input" type="checkbox" value="<?php echo esc_attr( $key ); ?>" <?php checked( $opt ); ?> />
								<?php echo esc_html( $choices[$key] ); ?>
							</label>
							<i class="dashicons dashicons-sort"></i>
						</li>

					<?php } // end choices. ?>

						<input id="<?php echo $this->id ; ?>" name="<?php echo $this->id ; ?>" type="hidden" <?php $this->link(); ?> class="tif-multicheck-sortable-input-hidden" value="<?php echo esc_attr ($hidden_input_value ); ?>" />

				</ul><!-- .tif-multicheck-sortable-list -->

			<?php

			}

		}

	}

}
