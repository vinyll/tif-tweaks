<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 */
if ( ! function_exists( 'tif_extend_image_radio_control_layout' ) ) {

	add_action( 'customize_register', 'tif_extend_image_radio_control_layout' );

	function tif_extend_image_radio_control_layout( $wp_customize ) {

		if ( ! class_exists( 'WP_Customize_Control' ) )
		return null;

		class Tif_Customize_Layout_Control extends WP_Customize_Control {

			public function render_content() {

				if ( empty( $this->choices ) )
					return;

				$name = '_customize-radio-' . $this->id;

				if ( ! empty( $this->label ) ) // add label if needed.
					echo '<span class="customize-control-title">' . esc_html( $this->label ) . '</span>';

				if ( ! empty( $this->description ) ) // add desc if needed.
					echo '<span class="description customize-control-description">' .  wp_kses( $this->description, tif_allowed_html() ) . '</span>';

				?>

				<ul class="tif-layout-control tif-radio-img">

				<?php

				$this_value  = $this->value();
				$this_value  = is_array( $this_value ) ? $this_value : explode( ',', $this_value);
				$this_layout = $this_value[0];

				foreach ( $this->choices as $value => $label ) :

					$src   = is_array( $label ) ? $label[0] : $label;
					$legend = is_array( $label ) ? (string)$label[1] : null;

					?>

					<li class="tif-radio-img-item">

						<input
						id="<?php echo esc_attr( $name ) . '_' . esc_attr( $value ); ?>"
						type="radio" value="<?php echo esc_attr( $value ); ?>"
						name="<?php echo esc_attr( $name ); ?>"
						class="tif-radio-img-item-input"

						<?php
						// $this->link();
						checked( $this_layout, $value );
						?>
						/>

						<label for="<?php echo esc_attr( $name ) . '_' . esc_attr( $value ); ?>">
							<img src="<?php echo esc_url( $src ); ?>" />
							<?php if ( null != $legend ) echo '<small>' . esc_attr( $legend ) . '</small>' ?>
						</label>

					</li>

					<?php

				endforeach;

				if ( isset( $this->input_attrs['additional_class'] ) ) {

					$i = 1;
					foreach ( $this->input_attrs['additional_class'] as $key ) {
						?>

						<li>

							<label>

								<?php echo esc_html( $key ) . ' - ' . __( 'Additional CSS Class(es)', 'canopee' ); ?>
								<input type="text" value="<?php echo( isset( $this_value[$i] ) ? esc_attr( $this_value[$i] ) : null ) ?>" />

							</label>

						</li>

						<?php
						++$i;
					}

				}

				?>

				<input type="hidden" <?php $this->link(); ?> value="<?php echo esc_attr( $this->value() ); ?>" />
				</ul>

				<?php
			}

		}

	}

}
