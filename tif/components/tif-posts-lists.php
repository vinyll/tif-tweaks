<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 */
function tif_get_callback_enabled( $enabled ) {

	$enabled = ! is_array( $enabled ) ? explode( ',', $enabled ) : $enabled;

	$result  = [];
	foreach ( $enabled as $key => $value ) {

		if ( strpos( $value, ':1') === false && strpos( $value, ':0') === false )
			$value = $value . ':1';

		list( $k, $v ) = explode( ':', $value );

		if ( $v != 0 )
			$result[$k] = $v;

	}

	return $result;

}

/**
 * [tif_get_loop_attr description]
 * @TODO
 */
function tif_get_loop_attr( $attr = array() ) {

	$attr = is_array( $attr ) ? $attr : tif_sanitize_loop_layout( $attr );

	$loop_attr = array(
		'layout'		=> isset( $attr['0'] ) ? (string)$attr['0'] : 'row',
		'thumbnail'		=> array(
			'size'			=> isset( $attr[2] ) && strpos( $attr[2], '.' ) === false ? (string)$attr[2] : 'tif-medium',
			'ratio'			=> isset( $attr[2] ) && strpos( $attr[2], '.' ) !== false ? (float)$attr[2] : '1.1',
			'attr'			=> array( 'class' => null )
		),
		'title'			=> array(
			'attr'			=> array( 'class' => ' col-span-full' )
		),
		'wrap_container'=> array(
			'wrap'			=> false,
			'attr'			=> array( 'class' => null ),
		),
		'container'		=> array(
			'grid'			=> isset( $attr[1] ) ? (int)$attr[1] : (int)1,
			'wrap'			=> 'div',
			'attr'			=> array( 'class' => null ),
			'additional'	=> array( 'class' => null ),
		),
		'inner'			=> array(
			'wrap'			=> false,
			'attr'			=> array( 'class' => null ),
			'additional'	=> array( 'class' => null ),
		),
		'post'			=> array(
			'attr'			=> array( 'class' => null ),
			'additional'	=> array( 'class' => null ),
			'inner'			=> array(
				'wrap'			=> false,
				'attr'			=> array(),
			),
			'wrap_content'		=> array(
				'wrap'			=> false,
				'attr'			=> array( 'class' => null ),
			),
		),
	);

	switch ( $loop_attr['layout'] ) {
		case 'inline':
		case 'cloud':
			$loop_attr['container']['attr']['class']			 = ' flex flex-wrap';
		break;

		case 'row':
			$loop_attr['container']['attr']['class']			 = ' tif-row sm:grid sm:grid-cols-' . (int)$loop_attr['container']['grid'];
			$loop_attr['post']['inner']['wrap']				 	 = 'div';
			$loop_attr['post']['inner']['attr']['class']		 = ' inner boxed sm:grid sm:grid-cols-3 gap-16';
			$loop_attr['post']['wrap_content']['wrap']			 = 'div';
			$loop_attr['post']['wrap_content']['attr']['class']	 = ' wrap-content col-span-2 flex flex-col gap-5';
		break;

		case 'row_50':
			$loop_attr['container']['attr']['class']			 = ' tif-row sm:grid sm:grid-cols-' . (int)$loop_attr['container']['grid'];
			$loop_attr['post']['inner']['wrap']					 = 'div';
			$loop_attr['post']['inner']['attr']['class']		 = ' inner boxed sm:grid sm:grid-cols-2 gap-16';
			$loop_attr['post']['wrap_content']['wrap']			 = 'div';
			$loop_attr['post']['wrap_content']['attr']['class']	 = ' wrap-content flex flex-col gap-5';
		break;

		case 'column':
			$loop_attr['container']['attr']['class']			 = ' tif-column sm:grid sm:grid-cols-' . (int)$loop_attr['container']['grid'];
			$loop_attr['post']['attr']['class']					 = ' flex flex-col gap-5';
		break;

		case 'grid_1':
			$loop_attr['container']['attr']['class']			 = ' grid grid-cols-1';
		break;

		case 'grid_2':
			$loop_attr['container']['attr']['class']			 = ' grid grid-cols-2';
		break;

		case 'grid_3':
			$loop_attr['container']['attr']['class']			 = ' grid grid-cols-3';
		break;

	}

	if ( isset( $attr['3'] ) && null != $attr['3'] )
		$loop_attr['container']['attr']['class'] .= ' ' . esc_attr( $attr['3'] );

	if ( isset( $attr['4'] ) && null != $attr['4'] )
		$loop_attr['post']['attr']['class']		 .= ' ' . esc_attr( $attr['3'] );

	return $loop_attr;

}

/**
 * Tif_loopable
 * TODO
 */
function tif_posts_loop( $args = array(), $callback = array(), $attr = array() ) {

	$tif_debug = null;

	$default_attr  = array(
		'size'			=> false,
		'title_loop'	=> array(
			'title'			=> false,
			'wrap'			=> false,
			'attr'			=> array(),
			'before'		=> '<span>',
			'after'			=> '</span>'
		),
		'wrap_container'=> array(
			'wrap'			=> false,
			'attr'			=> array(),
			'additional'	=> array()
		),
		'container'		=> array(
			'wrap'			=> false,
			'attr'			=> array(),
			'additional'	=> array()
		),
		'inner'			=> array(
			'wrap'			=> false,
			'attr'			=> array(),
			'additional'	=> array()
		),
		'post'			=> array(
			'attr'			=> array(),
			'additional'	=> array(),
			'inner'			=> array(
				'wrap'			=> false,
				'attr'			=> array(),
				'additional'	=> array()
			),
		),
		'nopost'		=> array(
			'wrap'			=> 'p',
			'attr'			=> array(),
			'content'		=> esc_html__( 'Nothing Found', 'canopee' ),
		),
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );
	$title		= $parsed['title_loop']['title'] && $parsed['title_loop']['wrap']
				? '<' . $parsed['title_loop']['wrap'] . tif_parse_attr( $parsed['title_loop']['attr'] ) . '>'
					. $parsed['title_loop']['before']
					. $parsed['title_loop']['title']
					. $parsed['title_loop']['after']
					. '</' . $parsed['title_loop']['wrap'] . '>'
				: false ;

	$wrap_container	= $parsed['wrap_container']['wrap'] ? '<' . $parsed['wrap_container']['wrap'] . tif_parse_attr( $parsed['wrap_container']['attr'] ) . '>' : false ;
	$container	= $parsed['container']['wrap'] ? '<' . $parsed['container']['wrap'] . tif_parse_attr( $parsed['container']['attr'], $parsed['container']['additional'] ) . '>' : false ;
	$inner		= isset( $parsed['inner']['wrap'] ) && $parsed['inner']['wrap'] ? '<' . $parsed['inner']['wrap'] . tif_parse_attr( $parsed['inner']['attr'], $parsed['inner']['additional'] ) . '>' : false ;
	$post_inner		= isset( $parsed['post']['inner']['wrap'] ) && $parsed['post']['inner']['wrap'] ? '<' . $parsed['post']['inner']['wrap'] . tif_parse_attr( $parsed['post']['inner']['attr'] ) . '>' : false ;

	/**
	 * Functions hooked in to ...
	 *
	 * @hooked ... - 10
	 */

	do_action( 'tif.loop.before' );

	// Open Wrap Container if there is one
	if ( $wrap_container ) echo $wrap_container . "\n\n" ;

	// Open Container if there is one
	if ( $container ) echo $container . "\n\n" ;

	// Open Inner if there is one
	if ( $inner ) echo $inner . "\n\n" ;

	// Title
	if ( $title ) echo $title . "\n\n" ;

	$count_loop = 0;

	/**
	 * Functions hooked in to ...
	 *
	 * @hooked ... - 10
	 */

	do_action( 'tif.loop.top' );

	if ( have_posts() ) {

		// The query
		// query_posts( $args);
		while ( have_posts() ) : the_post();

			if ( isset( $parsed['post']['attr']['class'] ) && is_array( $parsed['post']['attr']['class'] ) ) :
				$count			 = count( $parsed['post']['attr']['class'] );
				$post_attr_class = $parsed['post']['attr']['class'][$count_loop];
				$count_loop		 = $count_loop < $count - 1 ? ++$count_loop : 0 ;
			else :
				$post_attr_class = isset( $parsed['post']['attr']['class'] ) ? $parsed['post']['attr']['class'] : null ;
			endif;

			if ( isset( $parsed['post']['additional']['class'] ) && is_array( $parsed['post']['additional']['class'] ) ) :
				$count			 = count( $parsed['post']['additional']['class'] );
				$post_add_class	 = $parsed['post']['additional']['class'][$count_add_loop];
				$count_add_loop	 = $count_add_loop < $count - 1 ? ++$count_add_loop : 0 ;
			else :
				$post_add_class	 = isset( $parsed['post']['additional']['class'] ) ? $parsed['post']['additional']['class'] : null ;
			endif;

			tif_get_count_loop( 1 );

			// if ( isset( $callback['wrap_entry']['container']['layout'] ) )
			// 	$callback['wrap_entry']['container']['layout'] = $parsed['layout'] ;

			// The post
			echo '<article id="post-' . get_the_ID() . '" class="' . join( ' ', get_post_class( esc_attr( $post_attr_class . ' ' . $post_add_class ),  get_the_ID() ) ) . '">' . "\n\n" ;

			// Open Post Inner if there is one
			if ( $post_inner ) echo $post_inner . "\n\n" ;

			foreach ( $callback as $key => $value ) {

				// Prevent a notice
				if ( function_exists( 'tif_' . $key ) )
					call_user_func_array( 'tif_' . $key, array( $value ) );

				// DEBUG:
				if ( tif_is_loop_debug() )
					$tif_debug .= '<small>. tif_' . $key . '</small>' . "\n";

			}

			// Close Post Inner if there is one
			if ( $post_inner ) echo '</' . $parsed['post']['inner']['wrap'] . '>' . "\n\n" ;

			echo '</article>' . "\n\n" ;

			endwhile;

	} else {

		echo '<' . esc_html( $parsed['nopost']['wrap'] ) . tif_parse_attr( $parsed['nopost']['attr'] ) . '>';
		echo esc_html( $parsed['nopost']['content'] );
		echo '</' . esc_html( $parsed['nopost']['wrap'] ) . '>';

	}

	wp_reset_query();

	/**
	 * Functions hooked in to ...
	 *
	 * @hooked ... - 10
	 */

	do_action( 'tif.loop.bottom' );

	// Close Inner if there is one
	if ( $inner ) echo '</' . $parsed['inner']['wrap'] . '>' . "\n\n" ;

	// Close Container if there is one
	if ( $container ) echo '</' . $parsed['container']['wrap'] . '>' . "\n\n" ;

	tif_home_loop_debug_alert( __FUNCTION__, $tif_debug );

	/**
	 * Functions hooked in to ...
	 *
	 * @hooked ... - 10
	 */

	do_action( 'tif.loop.after' );


}

/**
 * Tif_loopable
 * TODO
 */
function tif_posts_query( $args = array(), $callback = array(), $attr = array() ) {

	$tif_debug	= null;

	$default_attr = array(
		'size'			=> false,
		'title_loop'	=> array(
			'title'			=> false,
			'wrap'			=> false,
			'attr'			=> array(),
			'before'		=> '<span>',
			'after'			=> '</span>'
		),
		'wrap_container'=> array(
			'wrap'			=> false,
			'attr'			=> array(),
			'additional'	=> array()
		),
		'container'		=> array(
			'wrap'			=> false,
			'attr'			=> array(),
			'additional'	=> array()
		),
		'inner'			=> array(
			'wrap'			=> false,
			'attr'			=> array(),
			'additional'	=> array()
		),
		'post'			=> array(
			'attr'			=> array(),
			'inner'			=> array(
				'wrap'			=> false,
				'attr'			=> array(),
				'additional'	=> array()
			),
		),
		'nopost'		=> array(
			'wrap'			=> 'p',
			'attr'			=> array(),
			'content'		=> esc_html__( 'Nothing Found', 'canopee' ),
		),
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	$title		= $parsed['title_loop']['title'] && $parsed['title_loop']['wrap']
				? '<' . $parsed['title_loop']['wrap'] . tif_parse_attr( $parsed['title_loop']['attr'] ) . '>'
					. $parsed['title_loop']['before']
					. $parsed['title_loop']['title']
					. $parsed['title_loop']['after']
					. '</' . $parsed['title_loop']['wrap'] . '>'
				: false ;
	$wrap_container	= $parsed['wrap_container']['wrap'] ? '<' . $parsed['wrap_container']['wrap'] . tif_parse_attr( $parsed['wrap_container']['attr'] ) . '>' : false ;
	$container	= $parsed['container']['wrap'] ? '<' . $parsed['container']['wrap'] . tif_parse_attr( $parsed['container']['attr'], $parsed['container']['additional'] ) . '>' : false ;
	$inner		= isset( $parsed['inner']['wrap'] ) && $parsed['inner']['wrap'] ? '<' . $parsed['inner']['wrap'] . tif_parse_attr( $parsed['inner']['attr'], $parsed['inner']['additional'] ) . '>' : false ;
	$post_inner		= isset( $parsed['post']['inner']['wrap'] ) && $parsed['post']['inner']['wrap'] ? '<' . $parsed['post']['inner']['wrap'] . tif_parse_attr( $parsed['post']['inner']['attr'] ) . '>' : false ;

	// Open Wrap Container if there is one
	if ( $wrap_container ) echo $wrap_container . "\n\n" ;

	// Open Container if there is one
	if ( $container ) echo $container . "\n\n" ;

	// Open Inner if there is one
	if ( $inner ) echo $inner . "\n\n" ;

	// Title
	if ( $title ) echo $title . "\n\n" ;

	$count_loop = 0;

	// The query
	$tif_the_query = new WP_Query( $args );

	if ( $tif_the_query->have_posts() ) {

		while( $tif_the_query->have_posts() ): $tif_the_query->the_post();

		if ( isset( $parsed['post']['attr']['class'] ) && is_array( $parsed['post']['attr']['class'] ) ) :
			$count			 = count( $parsed['post']['attr']['class'] );
			$post_attr_class = $parsed['post']['attr']['class'][$count_loop];
			$count_loop		 = $count_loop < $count - 1 ? ++$count_loop : 0 ;
		else :
			$post_attr_class = isset( $parsed['post']['attr']['class'] ) ? $parsed['post']['attr']['class'] : null ;
		endif;

		if ( isset( $parsed['post']['additional']['class'] ) && is_array( $parsed['post']['additional']['class'] ) ) :
			$count			 = count( $parsed['post']['additional']['class'] );
			$post_add_class	 = $parsed['post']['additional']['class'][$count_add_loop];
			$count_add_loop	 = $count_add_loop < $count - 1 ? ++$count_add_loop : 0 ;
		else :
			$post_add_class	 = isset( $parsed['post']['additional']['class'] ) ? $parsed['post']['additional']['class'] : null ;
		endif;

		tif_get_count_loop( 1 );

		// The post
		echo '<article id="post-' . get_the_ID() . '" class="' . join( ' ', get_post_class( esc_attr( $post_attr_class . ' ' . $post_add_class ),  get_the_ID() ) ) . '">' . "\n\n" ;

		// Open Post Inner if there is one
		if ( $post_inner ) echo $post_inner . "\n\n" ;

		foreach ( $callback as $key => $value ) {

			// Prevent a notice
			if ( function_exists( 'tif_' . $key ) )
				call_user_func_array( 'tif_' . $key, array( $value ) );

			// DEBUG:
			if ( tif_is_loop_debug() )
				$tif_debug .= '<small>. tif_' . $key . '</small>' . "\n";

		}

		// Close Post Inner if there is one
		if ( $post_inner ) echo '</' . $parsed['post']['inner']['wrap'] . '>' . "\n\n" ;

		echo '</article>' . "\n\n" ;

		endwhile;

	} else {

		echo '<' . esc_html( $parsed['nopost']['wrap'] ) . tif_parse_attr( $parsed['nopost']['attr'] ) . '>';
		echo esc_html( $parsed['nopost']['content'] );
		echo '</' . esc_html( $parsed['nopost']['wrap'] ) . '>';

	}

	wp_reset_postdata();

	// Close Inner if there is one
	if ( $inner ) echo '</' . $parsed['inner']['wrap'] . '>' . "\n\n" ;

	// Close Container if there is one
	if ( $container ) echo '</' . $parsed['container']['wrap'] . '>' . "\n\n" ;

	// Close Wrap Container if there is one
	if ( $wrap_container ) echo '</' . $parsed['wrap_container']['wrap'] . '>' . "\n\n" ;

	tif_home_loop_debug_alert( __FUNCTION__, $tif_debug );

}

/**
 * Tif_loopable
 * TODO
 */
function tif_wrap( $callback = array(), $attr = array() ) {

	$tif_debug	= null;

	if ( empty( $callback ) )
		return;

	$default_attr = array(
		'title'			=> array(
			'wrap'			=> false,
			'content'		=> false,
			'attr'			=> array(),
			'before'		=> '<span>',
			'after'			=> '</span>'
		),
		'wrap_container'=> array(
			'wrap'			=> false,
			'attr'			=> array(),
			'additional'	=> array()
		),
		'container'		=> array(
			'wrap'			=> false,
			'attr'			=> array(),
			'additional'	=> array()
		),
		'inner'			=> array(
			'wrap'			=> false,
			'attr'			=> array(),
			'additional'	=> array()
		),
		'hook'			=> false
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	if ( isset( $attr['container']['attr'][0] ) ) {

		global $count_wrap_loop;
		$count_wrap_loop			= null == $count_wrap_loop ? 0 : $count_wrap_loop;
		$count						= count( $attr['container']['attr'] );
		$parsed['container']['attr']= $attr['container']['attr'][$count_wrap_loop];
		$count_wrap_loop			= $count_wrap_loop < $count - 1 ? ++$count_wrap_loop : 0 ;

	}

	$title	= $parsed['title']['content'] && $parsed['title']['wrap']
				? '<' . $parsed['title']['wrap'] . tif_parse_attr( $parsed['title']['attr'] ) . '>'
					  . $parsed['title']['before']
					  . $parsed['title']['content']
					  . $parsed['title']['after']
				. '</' . $parsed['title']['wrap'] . '>'
				: false ;

	$wrap_container	= $parsed['wrap_container']['wrap'] ? '<' . $parsed['wrap_container']['wrap'] . tif_parse_attr( $parsed['wrap_container']['attr'] ) . '>' : false ;
	$container = $parsed['container']['wrap'] ? '<' . $parsed['container']['wrap'] . tif_parse_attr( $parsed['container']['attr'], $parsed['container']['additional'] ) . '>' : false ;
	$inner	= isset( $parsed['inner']['wrap'] ) && $parsed['inner']['wrap'] ? '<' . $parsed['inner']['wrap'] . tif_parse_attr( $parsed['inner']['attr'], $parsed['inner']['additional'] ) . '>' : false ;

	if ( $parsed['hook'] )
		do_action( 'tif.' . $parsed['hook'] . '.before' );

	// Open Wrap Container if there is one
	if ( $wrap_container ) echo $wrap_container . "\n\n" ;

	// Open Container if there is one
	if ( $container ) echo $container . "\n\n" ;

	// Title
	if ( $title ) echo $title . "\n\n" ;

		// Open Inner if there is one
		if ( $inner ) echo $inner . "\n\n" ;

		if ( $parsed['hook'] )
			do_action( 'tif.' . $parsed['hook'] . '.top' );

			foreach ( $callback  as $key => $value ) {

				// Prevent a notice
				if ( function_exists( 'tif_' . $key ) )
					call_user_func_array( 'tif_' . $key, array( $value ) );

				// DEBUG:
				if ( tif_is_loop_debug() )
					$tif_debug .= '<small>. tif_' . $key . '</small>' . "\n";

			}

			if ( $parsed['hook'] )
				do_action( 'tif.' . $parsed['hook'] . '.bottom' );

		// Close Inner if there is one
		if ( $inner ) echo '</' . $parsed['inner']['wrap'] . '>' . "\n\n" ;

	// Close Container if there is one
	if ( $container ) echo '</' . $parsed['container']['wrap'] . '>' . "\n\n" ;

	// Close Wrap Container if there is one
	if ( $wrap_container ) echo '</' . $parsed['wrap_container']['wrap'] . '>' . "\n\n" ;

	if ( $parsed['hook'] )
		do_action( 'tif.' . $parsed['hook'] . '.after' );

	tif_home_loop_debug_alert( __FUNCTION__, $tif_debug );

}

/**
 * Tif_loopable
 * TODO
 */
function tif_wrap_header( $attr = array() ) {

	$tif_debug = null;

	if ( ! isset( $attr['callback'] ) )
		return;

	$default_attr = array(
		'container'		=> array(
			'wrap'			=> 'header',
			'attr'			=> array( 'class' => 'entry-header' )
		),
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	if ( isset( $attr['container']['attr'][0] ) ) {

		global $count_wrap_loop;
		$count_wrap_loop			= null == $count_wrap_loop ? 0 : $count_wrap_loop;
		$count						= count( $attr['container']['attr'] );
		$parsed['container']['attr']= $attr['container']['attr'][$count_wrap_loop];
		$count_wrap_loop			= $count_wrap_loop < $count - 1 ? ++$count_wrap_loop : 0 ;

	}

	tif_wrap( $parsed['callback'], $parsed );

}

/**
 * Tif_loopable
 * TODO
 */
function tif_wrap_entry( $attr = array() ) {

	$tif_debug = null;

	if ( ! isset( $attr['callback'] ) )
		return;

	$default_attr = array(
		'container'		=> array(
			'layout'		=> false,
			'wrap'			=> 'div',
			'attr'			=> array( 'class' => 'wrap-content' )
		),
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	$loop = isset( $attr['loop'] ) ? $attr['loop'] : false;
	if ( ! has_post_thumbnail() && ! tif_is_blank_thumbnail( $loop ) )
		$parsed['container']['attr']['class'] = str_replace( 'col-span-2', '', $parsed['container']['attr']['class'] )  . ' col-span-full';

	if ( isset( $attr['container']['attr'][0] ) ) {

		global $count_wrap_loop;
		$count_wrap_loop			= null == $count_wrap_loop ? 0 : $count_wrap_loop;
		$count						= count( $attr['container']['attr'] );
		$parsed['container']['attr']= $attr['container']['attr'][$count_wrap_loop];
		$count_wrap_loop			= $count_wrap_loop < $count - 1 ? ++$count_wrap_loop : 0 ;

	}

	tif_wrap( $parsed['callback'], $parsed );

}

/**
 * Tif_loopable
 * TODO
 */
function tif_wrap_footer( $attr = array() ) {

	$tif_debug = null;

	if ( ! isset( $attr['callback'] ) )
		return;

	$default_attr = array(
		'container'		=> array(
			'wrap'			=> 'footer',
			'attr'			=> array( 'class' => 'entry-footer' )
		),
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	if ( isset( $attr['container']['attr'][0] ) ) {

		global $count_wrap_loop;
		$count_wrap_loop			= null == $count_wrap_loop ? 0 : $count_wrap_loop;
		$count						= count( $attr['container']['attr'] );
		$parsed['container']['attr']= $attr['container']['attr'][$count_wrap_loop];
		$count_wrap_loop			= $count_wrap_loop < $count - 1 ? ++$count_wrap_loop : 0 ;

	}

	tif_wrap( $parsed['callback'], $parsed );

}

/**
 * Tif_loopable
 * TODO
 */
function tif_post_cover_meta( $attr = array() ) {

	$tif_debug = null;

	if ( ! isset( $attr['callback'] ) )
		return;

	if ( tif_is_woocommerce_page() )
		return;

	$default_attr = array(
		'container'		=> array(
			'wrap'			=> 'div',
			'attr'			=> array( 'class' => 'entry-meta' )
		),
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	tif_wrap( $parsed['callback'], $parsed );

}

/**
 * Tif_loopable
 * TODO
 */
function tif_post_meta( $attr = array() ) {

	$tif_debug = null;

	if ( ! isset( $attr['callback'] ) )
		return;

	if ( tif_is_woocommerce_page() )
		return;

	$default_attr = array(
		'container'		=> array(
			'wrap'			=> 'div',
			'attr'			=> array( 'class' => 'entry-meta' )
		),
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	tif_wrap( $parsed['callback'], $parsed );

}

/**
 * Tif_loopable
 * TODO
 */
function tif_post_attachment( $attr = array() ) {

	global $post;

	$default_attr = array(
		'wrap_container'=> array(
			'wrap'			=> false,
			'attr'			=> array(),
			'additional'	=> array()
		),
		'container'		=> array(
			'wrap'			=> 'div',
			'attr'			=> array( 'class' => 'entry-attachment' ),
			'additional'	=> array()
		),
		'inner'			=> array(
			'wrap'			=> 'div',
			'attr'			=> array(),
			'additional'	=> array()
		),
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	$wrap_container	= $parsed['wrap_container']['wrap'] ? '<' . $parsed['wrap_container']['wrap'] . tif_parse_attr( $parsed['wrap_container']['attr'] ) . '>' : false ;
	$container = $parsed['container']['wrap'] ? '<' . $parsed['container']['wrap'] . tif_parse_attr( $parsed['container']['attr'], $parsed['container']['additional'] ) . '>' : false ;
	$inner	= isset( $parsed['inner']['wrap'] ) && $parsed['inner']['wrap'] ? '<' . $parsed['inner']['wrap'] . tif_parse_attr( $parsed['inner']['attr'], $parsed['inner']['additional'] ) . '>' : false ;

	// Open Wrap Container if there is one
	if ( $wrap_container ) echo $wrap_container . "\n\n" ;

	// Open Container if there is one
	if ( $container ) echo $container . "\n\n" ;

	// Open Inner if there is one
	if ( $inner ) echo $inner . "\n\n" ;

	$size = apply_filters( 'wporg_attachment_size', 'tif-large' );

		if ( wp_attachment_is_image( $post->id ) )
			echo tif_wp_get_attachment_image( get_the_ID(), $size );

		else
			echo '<a href="' . wp_get_attachment_url($post->ID) . '" title="' . wp_specialchars( get_the_title($post->ID), 1 ) .'" rel="attachment">' . basename($post->guid) . '</a>';

		if ( has_excerpt() ) :

			echo '<div class="entry-caption">';

			the_excerpt();

			echo '</div><!-- .entry-caption -->';

		endif;

	// Close container if there is one
	if ( $inner ) echo '</' . $parsed['inner']['wrap'] . '>' . "\n\n" ;

	// Close container if there is one
	if ( $container ) echo '</' . $parsed['container']['wrap'] . '>' . "\n\n" ;

	// Close Wrap Container if there is one
	if ( $wrap_container ) echo '</' . $parsed['wrap_container']['wrap'] . '>' . "\n\n" ;

}

/**
 * Tif_loopable
 * TODO
 */
function tif_post_author( $attr = array() ) {

	if ( tif_is_woocommerce_page() )
		return;

	global $post;

	$default_attr = array(
		'wrap_container'=> array(
			'wrap'			=> false,
			'attr'			=> array(),
			'additional'	=> array()
		),
		'container'		=> array(
			'wrap'			=> 'div',
			'attr'			=> array( 'class' => 'no-print post-author flex' ),
			'additional'	=> array()
		),
		'inner'			=> array(
			'wrap'			=> false,
			'attr'			=> false,
			'additional'	=> array()
		),
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	$wrap_container	= $parsed['wrap_container']['wrap'] ? '<' . $parsed['wrap_container']['wrap'] . tif_parse_attr( $parsed['wrap_container']['attr'] ) . '>' : false ;
	$container = $parsed['container']['wrap'] ? '<' . $parsed['container']['wrap'] . tif_parse_attr( $parsed['container']['attr'], $parsed['container']['additional'] ) . '>' : false ;
	$inner	= isset( $parsed['inner']['wrap'] ) && $parsed['inner']['wrap'] ? '<' . $parsed['inner']['wrap'] . tif_parse_attr( $parsed['inner']['attr'], $parsed['inner']['additional'] ) . '>' : false ;

	// Open Wrap Container if there is one
	if ( $wrap_container ) echo $wrap_container . "\n\n" ;

	// Open Container if there is one
	if ( $container ) echo $container . "\n\n" ;

	// Open Inner if there is one
	if ( $inner ) echo $inner . "\n\n" ;

	$id_author = get_post_field( 'post_author', $post->ID );
	$author_nickname = get_user_meta( $id_author, 'nickname', true );

	$has_social = false ;
	$social = array(
		'facebook'	=> get_the_author_meta( 'facebook', $post->post_author ),
		'twitter'	=> get_the_author_meta( 'twitter', $post->post_author ),
		'linkedin'	=> get_the_author_meta( 'linkedin', $post->post_author ),
		'youtube'	=> get_the_author_meta( 'youtube', $post->post_author ),
		'diaspora'	=> get_the_author_meta( 'diaspora', $post->post_author ),
		'mastodon'	=> get_the_author_meta( 'mastodon', $post->post_author ),
		'peertube'	=> get_the_author_meta( 'peertube', $post->post_author ),
	);
	$build_social = null ;
	foreach ( $social as $key => $value ) {

		if ( $value ) :

			$build_social .= '<li class="' . esc_attr( $key ) . '"><a href="' . esc_html( $value ) . '" target="_blank" rel="noreferrer noopener"><i class="fa fa-' . esc_attr( $key ) . '" aria-hidden="true"></i><span class="screen-reader-text">' . esc_attr( $key ) . '</span></a></li>';
			$has_social = true;

		endif;

	}
	$socialbtn = $has_social ? '<ul class="is-unstyled tif-social-links small">' . $build_social . '</ul>' : null ;

	if ( null != $author_nickname ) :

		$avatar = get_tif_local_avatar( get_post_field( 'post_author', $post->ID ) );
		$author = '<span class="vcard h4-like"><a href="' . get_author_posts_url( $id_author ) . '" title="View all posts by ' . get_the_author_meta( 'display_name', $id_author) . '" rel="author">' . get_the_author_meta( 'display_name', $id_author) . '</a></span>';
		$description = '<p class="description">' . get_user_meta( $id_author, 'description', true ) . '</p>';
		// description .= $meta['description'][0]

		$entry  = '<div class="avatar">' . $avatar . '</div>';
		$entry .= '<div class="biography">' . $author . $description . $socialbtn .'</div>';

		echo $entry ;

	endif;

	// Close container if there is one
	if ( $inner ) echo '</' . $parsed['inner']['wrap'] . '>' . "\n\n" ;

	// Close container if there is one
	if ( $container ) echo '</' . $parsed['container']['wrap'] . '>' . "\n\n" ;

	// Close Wrap Container if there is one
	if ( $wrap_container ) echo '</' . $parsed['wrap_container']['wrap'] . '>' . "\n\n" ;

}

/**
 * Tif_loopable
 * TODO
 */
function tif_meta_avatar() {

	$author = null;

	if ( 'post' == get_post_type() && in_the_loop() ) {

		if ( null != get_the_author_meta( 'nickname' ) )
		/* translators: 1: Post author url, 2: title tag for the link, 3: avatar */
			$author = sprintf( '<span class="author avatar"><a href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
				esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
				esc_attr( sprintf(
					/* translators: %s: post author */
					esc_html__( 'View all posts by %s', 'canopee' ), get_the_author() )
				),
				get_tif_local_avatar( get_the_author_meta( 'ID' ), 32 )
			);

	} else {

		global $post;
		$id_author = get_post_field( 'post_author', $post->ID );
		$author_nickname = get_user_meta( $id_author, 'nickname', true );

		if ( null != $author_nickname )
			$author = '<span class="author avatar"><a href="' . get_author_posts_url( $id_author ) . '" title="View all posts by ' . get_the_author_meta( 'display_name', $id_author) . '" rel="author">' . get_tif_local_avatar( $id_author, 32 ) . '</a></span>';

	}

	echo $author;

}

/**
 * Tif_loopable
 * TODO
 */
function tif_meta_author() {

	$author = null;

	if ( 'post' == get_post_type() && in_the_loop() ) {

		if ( null != get_the_author_meta( 'nickname' ) )
			$author = sprintf( '<span class="author vcard"><a href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
				esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
				esc_attr( sprintf( esc_html__( 'View all posts by %s', 'canopee' ), get_the_author() ) ),
				get_the_author()
			);

	} else {

		global $post;
		$id_author = get_post_field( 'post_author', $post->ID );
		$author_nickname = get_user_meta( $id_author, 'nickname', true );

		if ( null != $author_nickname )
			$author = '<span class="author vcard"><a href="' . get_author_posts_url( $id_author ) . '" title="View all posts by ' . get_the_author_meta( 'display_name', $id_author) . '" rel="author">' . get_the_author_meta( 'display_name', $id_author) . '</a></span>';

	}

	if ( class_exists( 'Tif_Tweaks_Init' ) ) {

		$tweak_author = tif_get_option( 'plugin_tweaks', 'tif_callback,author_disabled', 'key' );

		if ( ( $tweak_author == 'unused' && count_user_posts( get_the_author_meta( 'ID' ) < 1 ) ) || $tweak_author == 'all' )
		$author = wp_kses(
			$author,
			array(
				'span' => array(
					'class' => array(),
				),
			)
		);

	}

	echo $author;

}

/**
 * Tif_loopable
 * TODO
 */
function tif_meta_published() {

	if ( ! tif_is_meta_published() )
		return;

	if ( has_post_format( array( 'chat', 'status' ) ) )
	/* translators: 1: post format name. 2: date */
		$format_prefix = __( '%1$s on %2$s', 'canopee' );

	else
		$format_prefix = '%2$s';

	$date = sprintf(
		/* translators: 1: publication date, 2: formatted publication date */
		'<span class="date"><time class="entry-date" itemprop="datePublished" datetime="%1$s">%2$s</time></span> ',
		esc_attr( get_the_date( 'c' ) ),
		esc_html( sprintf(
			$format_prefix,
			get_post_format_string( get_post_format() ),
			get_the_date()
			)
		)
	);

	echo $date;

}

/**
 * Prints HTML with meta information for the current post-date/time and author.
 * Tif_loopable
 * TODO
 */
function tif_meta_modified() {

	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {

		$time_string = '<time class="entry-modified" itemprop="dateModified" datetime="%1$s">%2$s</time>';

		$time_string = sprintf(
			$time_string,
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);

		$modified_on = sprintf(
			esc_html_x( 'Last update on %s', 'post update', 'canopee' ),
			$time_string
		);

		$modified = '<span class="date modified">' . $modified_on . '</span>';

		echo $modified;

	}

}

/**
 * Tif_loopable
 * TODO
 */
function tif_meta_category( $attr = array() ) {

	$categories = get_the_category_list( ', ' );

	if ( $categories ) :

		$categories = '<span class="categories-links">' . $categories . '</span> ';

		echo $categories;

	endif;

}

/**
 * Tif_loopable
 * TODO
 */
function tif_meta_comments( $attr = array() ) {

	if ( get_comments_number() < 1 )
		return;

	$default_attr = array(
		'type'			=> 'icons',
		'container'		=> array(
			'wrap'			=> 'span',
			'attr'			=> array( 'class' => 'entry-comments' ),
			'additional'	=> array()
		),
		'inner'			=> array(
			'wrap'			=> false,
			'attr'			=> array(),
			'additional'	=> array()
		)
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	$container		= $parsed['container']['wrap'] ? '<' . $parsed['container']['wrap'] . tif_parse_attr( $parsed['container']['attr'], $parsed['container']['additional'] ) . '>' : false ;
	$container_close = $container ? '</' . $parsed['container']['wrap'] . '>' : false ;

	$inner			= $parsed['inner']['wrap'] ? '<' . $parsed['inner']['wrap'] . tif_parse_attr( $parsed['inner']['attr'], $parsed['inner']['additional'] ) . '>' : false ;
	$inner_close	= $inner ? '</' . $parsed['inner']['wrap'] . '>' : false ;

	$comments = $container . $inner . "\n";

	if ( $parsed['type'] == "text" )
		$comments .= get_comments_number_text( esc_html__( 'No Comments', 'canopee' ), esc_html__( '1 Comment', 'canopee' ), esc_html__( '% Comments', 'canopee' ) );

	else
		$comments .= '<i class="fa fa-comments-o" aria-hidden="true"></i> ' . get_comments_number_text( '0', '1', '%' );

	$comments .= $container_close . $inner_close . "\n";

	echo $comments;

}

/**
 * Tif_loopable
 * TODO
 */
function tif_title() {

	if ( tif_is_woocommerce_activated() && is_shop() ) :
		echo '<h1 class="page-title">' . woocommerce_page_title( false ) . '</h1>';

	elseif ( is_archive() ) :

		if ( is_author()  || is_date() )
			the_archive_title( '<h1 class="page-title">', '</h1>' );

		else
			echo single_cat_title( '<h1 class="page-title">' ) . '</h1>';

	elseif ( is_search() ) :
		echo '<h1 class="page-title">' . esc_html__( 'Search Results for', 'canopee' ) . " \"" . get_search_query()."\"" . '</h1>';

	elseif ( is_404() ) :
		echo '<h1 class="page-title">' . esc_html__( 'That page can&rsquo;t be found', 'canopee' ) . '</h1>';

	else :
		return null;

	endif;

}

/**
 * Tif_loopable
 * TODO
 */
function tif_post_title( $attr = array() ) {

	$default_attr = array(
		'title'			 => array(
			'wrap'			=> 'h1',
			'attr'			=> array( 'class' => 'entry-title' ),
		),
		'wrap_container' => array(
			'wrap'			=> false,
			'attr'			=> array(),
			'additional'	=> array()
		),
		'container'		 => array(
			'wrap'			=> false,
			'attr'			=> array(),
			'additional'	=> array()
		),
		'inner'			 => array(
			'wrap'			=> false,
			'attr'			=> array(),
			'additional'	=> array()
		)
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	$container = $container_close = null;

	$container		 .= $parsed['wrap_container']['wrap'] ? '<' . $parsed['wrap_container']['wrap'] . tif_parse_attr( $parsed['wrap_container']['attr'] ) . '>' : false ;
	$container		 .= $parsed['container']['wrap'] ? '<' . $parsed['container']['wrap'] . tif_parse_attr( $parsed['container']['attr'], $parsed['container']['additional'] ) . '>' : false ;
	$container		 .= $parsed['inner']['wrap'] ? '<' . $parsed['inner']['wrap'] . tif_parse_attr( $parsed['inner']['attr'], $parsed['inner']['additional'] ) . '>' : false ;

	$container_close .= $parsed['wrap_container']['wrap'] ? '</' . $parsed['wrap_container']['wrap'] . '>' : false ;
	$container_close .= $parsed['container']['wrap'] ? '</' . $parsed['container']['wrap'] . '>' : false ;
	$container_close .= $parsed['inner']['wrap'] ? '</' . $parsed['inner']['wrap'] . '>' : false ;

	$title			= $parsed['title']['wrap'] ? '<' . $parsed['title']['wrap'] . tif_parse_attr( $parsed['title']['attr'] ) . '>' : false ;
	$title_close	= $title ? '</' . $parsed['title']['wrap'] . '>' : false ;

	do_action( 'tif.post_title.before' );

	if ( is_home() && ! is_front_page() )
		echo $container . $title . get_queried_object()->post_title . $title_close . $container_close;

	else
		the_title( $container . $title, $title_close . $container_close );

	do_action( 'tif.post_title.after' );
}

/**
 * Tif_loopable
 * TODO
 */
function tif_post_title_loop( $attr = array() ) {

	if ( isset( $attr['link']) && is_bool( $attr['link'] ) && $attr['link'] )
		unset( $attr['link'] );

	$default_attr = array(
		'title'			=> array(
			'wrap'			=> 'header',
			'attr'			=> array( 'class' => 'entry-title h4-like' ),
		),
		'link'			=> array(
			'url'			=> get_permalink(),
			'attr'			=> array( 'rel' => 'bookmark' ),
		),
		'container'		=> array(
			'wrap'			=> false,
			'attr'			=> array(),
			'additional'	=> array()
		),
		'inner'			=> array(
			'wrap'			=> false,
			'attr'			=> array(),
			'additional'	=> array()
		)
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	$container		= $parsed['container']['wrap'] ? '<' . $parsed['container']['wrap'] . tif_parse_attr( $parsed['container']['attr'], $parsed['container']['additional'] ) . '>' : false ;
	$container_close = $container ? '</' . $parsed['container']['wrap'] . '>' : false ;

	$inner			= $parsed['inner']['wrap'] ? '<' . $parsed['inner']['wrap'] . tif_parse_attr( $parsed['inner']['attr'], $parsed['inner']['additional'] ) . '>' : false ;
	$inner_close	= $inner ? '</' . $parsed['inner']['wrap'] . '>' : false ;

	$title			= $parsed['title']['wrap'] ? '<' . $parsed['title']['wrap'] . tif_parse_attr( $parsed['title']['attr'] ) . '>' : false ;
	$title_close	= $title ? '</' . $parsed['title']['wrap'] . '>' : false ;

	$link			= $parsed['link'] ? '<a href="' . esc_url( $parsed['link']['url'] ) . '" ' . tif_parse_attr( $parsed['link']['attr'] ) . '>' : false ;
	$link_close		= $link ? '</a>' : false ;

	do_action( 'tif.post_title_loop.before' );
	the_title( $container . $inner . $title . $link, $link_close . $title_close . $inner_close . $container_close );
	do_action( 'tif.post_title_loop.after' );

}

/**
 * Tif_loopable
 * TODO
 */
function tif_post_tags( $attr = array() ) {

	$is_main_tags = is_singular() && tif_get_count_loop() === 1 ? ' entry-tags-main' : '' ;
	$class_tags = tif_get_option( 'theme_post_tags', 'tif_post_tags_colors,bgcolor', 'multicheck' );
	$default_attr = array(
		'wrap_container'=> array(
			'wrap'			=> false,
			'attr'			=> array(),
			'additional'	=> array()
		),
		'container'		=> array(
			'wrap'			=> 'div',
			'attr'			=> array( 'class' => 'entry-tags' . $is_main_tags ),
			'additional'	=> array()
		),
		'inner'			=> array(
			'wrap'			=> false,
			'attr'			=> array(),
			'additional'	=> array()
		),
		'ul'			=> array(
			'attr'			=> tif_post_tags_list_class( 'flex flex-wrap is-unstyled tif-tax-list' ),
		),
		'li'			=> array(
			'attr'			=> array(
				'class' 		=> 'has-tif-' . tif_esc_css( $class_tags[0] ) . '-background-color',
				'style'			=> false
			),
		),
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	$wrap_container	= $parsed['wrap_container']['wrap'] ? '<' . $parsed['wrap_container']['wrap'] . tif_parse_attr( $parsed['wrap_container']['attr'] ) . '>' : false ;
	$container = $parsed['container']['wrap'] ? '<' . $parsed['container']['wrap'] . tif_parse_attr( $parsed['container']['attr'], $parsed['container']['additional'] ) . '>' : false ;
	$inner	= isset( $parsed['inner']['wrap'] ) && $parsed['inner']['wrap'] ? '<' . $parsed['inner']['wrap'] . tif_parse_attr( $parsed['inner']['attr'], $parsed['inner']['additional'] ) . '>' : false ;

	$tags = get_the_tag_list(
		'<ul ' . tif_esc_css( tif_parse_attr( $parsed['ul']['attr'] ) ) . '><li ' . tif_esc_css( tif_parse_attr( $parsed['li']['attr'] ) ) . '>',
		'</li><li ' . tif_esc_css( tif_parse_attr( $parsed['li']['attr'] ) ) . '>',
		'</li></ul>'
	);

	if ( ! $tags )
		return;

	// Open Wrap Container if there is one
	if ( $wrap_container ) echo $wrap_container . "\n\n" ;

	// Open Container if there is one
	if ( $container ) echo $container . "\n\n" ;

	// Open Inner if there is one
	if ( $inner ) echo $inner . "\n\n" ;

			echo $tags ;

	// Close container if there is one
	if ( $inner ) echo '</' . $parsed['inner']['wrap'] . '>' . "\n\n" ;

	// Close container if there is one
	if ( $container ) echo '</' . $parsed['container']['wrap'] . '>' . "\n\n" ;

	// Close Wrap Container if there is one
	if ( $wrap_container ) echo '</' . $parsed['wrap_container']['wrap'] . '>' . "\n\n" ;

}

/**
 * Tif_loopable
 * TODO
 */
function tif_post_excerpt( $attr = array() ) {

	global $post;

	if ( ! $post->post_excerpt )
		return;

	$default_attr = array(
		'container'		=> array(
			'wrap'			=> 'div',
			'attr'			=> array( 'class' => 'entry-excerpt' ),
			'additional'	=> array()
		),
		'inner'			=> array(
			'wrap'			=> 'p',
			'attr'			=> array(),
			'additional'	=> array()
		),
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	$container = $parsed['container']['wrap'] ? '<' . $parsed['container']['wrap'] . tif_parse_attr( $parsed['container']['attr'], $parsed['container']['additional'] ) . '>' : false ;
	$inner	= isset( $parsed['inner']['wrap'] ) && $parsed['inner']['wrap'] ? '<' . $parsed['inner']['wrap'] . tif_parse_attr( $parsed['inner']['attr'], $parsed['inner']['additional'] ) . '>': false ;

	// Open Container if there is one
	if ( $container ) echo $container . "\n\n" ;

		// Open Inner if there is one
		if ( $inner ) echo $inner . "\n\n" ;

			echo strip_tags( strip_shortcodes( $post->post_excerpt ) );

		// Close container if there is one
		if ( $inner ) echo '</' . $parsed['inner']['wrap'] . '>' . "\n\n" ;

	// Close container if there is one
	if ( $container ) echo '</' . $parsed['container']['wrap'] . '>' . "\n\n" ;

}

/**
 * Tif_loopable
 * TODO
 */
function tif_post_content( $attr = array() ) {

	$default_attr = array(
		'wrap_container'=> array(
			'wrap'			=> false,
			'attr'			=> array(),
			'additional'	=> array()
		),
		'container'		=> array(
			'wrap'			=> 'div',
			'attr'			=> array( 'class' => 'entry-content clearfix' ),
			'additional'	=> array()
		),
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	$wrap_container	= $parsed['wrap_container']['wrap'] ? '<' . $parsed['wrap_container']['wrap'] . tif_parse_attr( $parsed['wrap_container']['attr'] ) . '>' : false ;
	$container	= $parsed['container']['wrap'] ? '<' . $parsed['container']['wrap'] . tif_parse_attr( $parsed['container']['attr'], $parsed['container']['additional'] ) . '>' : false ;
	$inner		= isset( $parsed['inner']['wrap'] ) && $parsed['inner']['wrap'] ? '<' . $parsed['inner']['wrap'] . tif_parse_attr( $parsed['inner']['attr'], $parsed['inner']['additional'] ) . '>' : false ;

	$length		= array_key_exists( 'length', $attr ) ? $attr['length'] : false ;
	$loop		= array_key_exists( 'loop', $attr ) ? $attr['loop'] : false ;
	$is_excerpt = array_key_exists( 'excerpt', $attr ) ? (bool)$attr['excerpt'] : false ;

	// Open Wrap Container if there is one
	if ( $wrap_container ) echo $wrap_container . "\n\n" ;

	// Open Container if there is one
	if ( $container ) echo $container . "\n\n" ;

	// Open Inner if there is one
	if ( $inner ) echo $inner . "\n\n" ;

	do_action( 'tif.content.top' );

	global $post;

	$content = strip_shortcodes( $post->post_content );

	if ( $is_excerpt && ! empty( $post->post_excerpt ) )
		$content = strip_tags( strip_shortcodes( $post->post_excerpt ) );

	if ( is_home() || is_front_page() || is_archive() || is_search() ) {

		if ( $length == 'more' ) {

			// Let's use <!--more--> tag
			if ( strstr( $post->post_content, '<!--more-->' ) ) {
				// <!--more--> tag exists, we're using it
				the_content();
			} else {
				// <!--more--> tag does not exists, then we get the first paragraph
				$content = apply_filters( 'the_content', get_the_content() );
				$text = substr( $content, 0, strpos( $content, '</p>' ) + 4 );
				echo $text;
			}

		} elseif ( $length == 'full' ) {

			// Display full content
			global $more;
			$more = -1;
			the_content();

		} else {

			// Trim content after a max number of words
			echo tif_trim_words( $content, (int)$length, 'html' );
			// echo tif_read_more_link();

		}

	} elseif ( is_single() && $length != 'full' ) {

		echo tif_trim_words( $content, (int)$length, 'html' );

	} else {

		the_content();

	}

	tif_edit_post_link();

	do_action( 'tif.content.bottom' );

	// Close container if there is one
	if ( $inner ) echo '</' . $parsed['inner']['wrap'] . '>' . "\n\n" ;

	// Close container if there is one
	if ( $container ) echo '</' . $parsed['container']['wrap'] . '>' . "\n\n" ;

	// Close Wrap Container if there is one
	if ( $wrap_container ) echo '</' . $parsed['wrap_container']['wrap'] . '>' . "\n\n" ;

}

/**
 * Tif_loopable
 * TODO
 */
function tif_post_thumbnail( $attr = array() ) {

	if ( empty( $attr ) )
		return;

	$default_attr  = array(
		'size'			=> false,
		'sizes'			=> array(
			'width'			=> null,
			'height'		=> null,
			'crop'			=> false,
		),
		'link'			=> array(
			'url'			=> get_permalink(),
			'attr'			=> array( 'rel' => 'nofollow' ),
		),
		'thumbnail'		=> array(
			'attr'			=> array( 'alt' => get_the_title(), 'title' => get_the_title() ),
		),
		'wrap_container'=> array(
			'wrap'			=> false,
			'attr'			=> array(),
			'additional'	=> array()
		),
		'container'		=> array(
			'wrap'			=> 'div',
			'attr'			=> array( 'class' => ' entry-thumbnail ' . get_post_format() ),
			'additional'	=> array()
		),
		'inner'			=> array(
			'wrap'			=> 'div',
			'attr'			=> array( 'class' => 'thumbnail', 'style' => false ),
			'additional'	=> array()
		),
		'blank'				=> false,
		'object'			=> false,
		'background'		=> false,

	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	$size		 = $parsed['size'];
	$width		 = $parsed['sizes']['width'];
	$height		 = $parsed['sizes']['height'];
	$crop		 = $parsed['sizes']['crop'];
	$link_url	 = $parsed['link']['url'] ;
	$link_attr	 = $parsed['link']['attr'] ;
	$thumb_alt	 = $parsed['thumbnail']['attr']['alt'] ;
	$thumb_title = $parsed['thumbnail']['attr']['title'] ;
	$blank		 = (bool)$parsed['blank'];
	$loop		 = isset( $parsed['loop'] ) ? $parsed['loop'] : false;

	if (	$size == 'tif-large'
		&& is_single() && in_the_loop()
		&& ( null != tif_get_option( 'theme_images', 'tif_images_ratio,single', 'string' ) )
		)
		$size = 'tif-single';

	if ( ! has_post_thumbnail() && ! tif_is_blank_thumbnail( $loop ) && ! $blank )
		return;

	if ( is_array( $parsed['size'] ) ) :
		global $thumb_count_loop;
		$thumb_count_loop	= null == $thumb_count_loop ? 0 : $thumb_count_loop;
		$count				= count( $parsed['size'] );
		$size				= $parsed['size'][$thumb_count_loop];
		$thumb_count_loop	= $thumb_count_loop < $count - 1 ? ++$thumb_count_loop : 0 ;
	endif;

	if ( ! $size )
		return;

	// Get thumbnail elements
	$post_id	= get_the_ID();
	$attachement_id	= get_post_thumbnail_id( $post_id );
	$attr		= array(
		'class'		=> $size,
		'alt'		=> $thumb_alt,
		'title'		=> $thumb_title,
		'sizes'		=> array(
			'width' 		=> (int)$width,
			'height'		=> (int)$height,
			'crop'			=> $crop
		),
		// 'blank'		=> ! has_post_thumbnail() ? true : false,
	) + ( tif_get_lazy_attr() );
	$blank		= ! has_post_thumbnail() ? true : false ;

	$attachement_url = $attachement = null;
	if ( $attachement_id || $loop ) {

		if ( $size == 'lazy' ) {

			tif_lazy_thumbnail( $attachement_id, null, $attr , array( $blank ) );
			$attachement_url = wp_get_attachment_image_url( (int)$attachement_id, 'tif_lazy_' . (int)$width . 'x' .(int)$height );
			$attachement = $attachement_url;

		} else {

			$attachement = tif_get_the_post_thumbnail( $attachement_id, null, $size, $attr, $blank );
			if ( isset( $parsed['background']['attachment'] ) && $parsed['background']['attachment'] )
				$attachement_url = tif_get_the_post_thumbnail_url( $attachement_id, null, $size );

		}

	}
	// END Get thumbnail elements

	// Has Object fit
	if ( isset( $parsed['object']['fit'] ) && $parsed['object']['fit'] )
		$parsed['container']['attr']['class'] .= ' has-tif-object-fit-' . esc_attr( $parsed['object']['fit'] );

	if ( isset( $parsed['object']['sizes'] ) )
		foreach ( $parsed['object']['sizes'] as $key => $value ) {
			$parsed['container']['attr']['style'] .= tif_sanitize_slug( $key ) . ':' . esc_attr( $value ) . ';';
		}

	// Has background attachement
	if ( isset( $parsed['background']['attachment'] ) && $parsed['background']['attachment'] && $attachement_url ) {
		$parsed['container']['attr']['class'] .= ' has-tif-background-attachment has-tif-' . esc_attr ($parsed['background']['attachment'] ) . '-background-attachment';
		$parsed['container']['attr']['style'] .= ' background-image:url(' . $attachement_url . ');';
	}

	$wrap_container	= $parsed['wrap_container']['wrap'] ? '<' . $parsed['wrap_container']['wrap'] . tif_parse_attr( $parsed['wrap_container']['attr'] ) . '>' : false ;
	$container	 = $parsed['container']['wrap'] ? '<' . $parsed['container']['wrap'] . tif_parse_attr( $parsed['container']['attr'], $parsed['container']['additional'] ) . '>' : false ;
	$inner		 = isset( $parsed['inner']['wrap'] ) && $parsed['inner']['wrap'] ? '<' . $parsed['inner']['wrap'] . tif_parse_attr( $parsed['inner']['attr'], $parsed['inner']['additional'] ) . '>' : false ;

	do_action( 'tif.post_thumbnail.before' );

	// Open Wrap Container if there is one
	if ( $wrap_container ) echo $wrap_container . "\n\n" ;

	// Open Container if there is one
	if ( $container ) echo $container . "\n\n" ;

		do_action( 'tif.post_thumbnail.top' );

			if ( ! is_singular() || $link_url )
				echo '<a href="' . esc_url( $link_url ) . '" ' . tif_parse_attr( $link_attr) . '>' . "\n\n" ;

				// Open Inner if there is one
				if ( $inner ) echo $inner . "\n\n" ;

					if ( $size == 'lazy' ) {
						echo '<img
							alt="' . esc_html( $alt ) . '"
							src="' . esc_url( $attachement_url ) . '"
							class=""
							width="' . (int)$width . '"
							height="' . (int)$height . '"
							 />';
					} else {
						echo $attachement;
					}

					echo '<div class="hover"><span></span></div>' . "\n\n" ;

				// Close Inner if there is one
				if ( $inner ) echo '</' . $parsed['inner']['wrap'] . '>' . "\n\n" ;

			if ( ! is_singular() || $link_url )
			echo '</a>' . "\n\n" ;

		do_action( 'tif.post_thumbnail.bottom' );

	// Close Container if there is one
	if ( $container ) echo '</' . $parsed['container']['wrap'] . '>' . "\n\n" ;

	// Close Wrap Container if there is one
	if ( $wrap_container ) echo '</' . $parsed['wrap_container']['wrap'] . '>' . "\n\n" ;

	do_action( 'tif.post_thumbnail.after' );

}

/**
 * Show Recent Comments
 *
 * Tif_loopable
 * TODO ARRAY CONTAINER / INNER
 *
 * @param string/integer $parsed['number']
 * @param string/integer $parsed['length']
 * @param string/integer $parsed['avatar']['width']
 *
 * @echo string $comm
 */
function tif_latest_comments( $args = array() ) {

	$default_args	= array(
		'number'		=> 5,
		'length'		=> 80,
		'avatar'		=> array(
			'width'			=> 48
		)
	);

	$parsed = tif_parse_args_recursive( $args, $default_args );

	$comments_query = new WP_Comment_Query();
	$comments = $comments_query->query( array( 'number' => (int)$parsed['number'] ) );

	if ( $comments ) :

		$comm = '';

		foreach ( $comments as $comment ) :

			$comm .= '<article>';
			$comm .= '<header class="entry-header">';
			$comm .= get_avatar( $comment->comment_author_email, $parsed['avatar']['width'] );
			$comm .= '</header>';
			$comm .= '<div class="wrap-content">';
			$comm .= '<a class="author" href="' . get_permalink( $comment->comment_post_ID ) . '#comment-' . (int)$comment->comment_ID . '"  rel="author">';
			$comm .= get_comment_author( $comment->comment_ID );
			$comm .= ' :</a>';
			$comm .= strip_tags( substr( apply_filters( 'get_comment_text', esc_html( $comment->comment_content ) ), 0, (int)$parsed['length'] ) );
			$comm .= '...</div>';
			$comm .= '</article>';

		endforeach;

	else :

		$comm .= 'No comments.';

	endif;

	echo $comm;
}

/**
 * Tif_loopable
 * TODO ARRAY CONTAINER / INNER
 */
function tif_post_read_more() {

	echo '<p><a class="more-link" href="' . get_permalink() . '" rel="nofollow" >' . esc_html__( 'Read now', 'canopee' ) . '</a></p>';

}

/**
 * Post navigation
 *
 * Tif_loopable
 * TODO ARRAY CONTAINER / INNER
 *
 * Display navigation to next/previous post when applicable.
 *
 * @link https://developer.wordpress.org/themes/functionality/pagination/
 * @link https://developer.wordpress.org/reference/functions/previous_post_link/
 * @link https://developer.wordpress.org/reference/functions/next_post_link/
 *
 * @origin the_post_navigation();
 * @link https://developer.wordpress.org/reference/functions/the_post_navigation/
 */
function tif_post_navigation( $attr = array() ) {

	$default_attr  = array(
		'wrap_container'	=> array(
			'wrap'				=> false,
			'attr'				=> array()
		),
		'container'			=> array(
			'wrap'				=> 'nav',
			'attr'				=> array(
				'class'				=> 'no-print navigation post-navigation',
				'role'				=> 'navigation',
			),
			'additional'		=> array()
		),
		'inner'				=> array(
			'wrap'				=> false,
			'attr'				=> array(),
			'additional'		=> array()
		),
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	$wrap_container	= $parsed['wrap_container']['wrap'] ? '<' . $parsed['wrap_container']['wrap'] . tif_parse_attr( $parsed['wrap_container']['attr'] ) . '>' : false ;
	$container	= $parsed['container']['wrap'] ? '<' . $parsed['container']['wrap'] . tif_parse_attr( $parsed['container']['attr'], $parsed['container']['additional'] ) . '>' : false ;
	$inner		= isset( $parsed['inner']['wrap'] ) && $parsed['inner']['wrap'] ? '<' . $parsed['inner']['wrap'] . tif_parse_attr( $parsed['inner']['attr'], $parsed['inner']['additional'] ) . '>' : false ;

	// Don't print empty markup if there's nowhere to navigate.
	$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
	$next	= get_adjacent_post( false, '', false );

	if ( ! $next && ! $previous )
		return;

	// Open Wrap Container if there is one
	if ( $wrap_container ) echo $wrap_container . "\n\n" ;

	// Open Container if there is one
	if ( $container ) echo $container . "\n\n" ;

	// Open Inner if there is one
	if ( $inner ) echo $inner . "\n\n" ;

	echo '<h3 class="screen-reader-text">' . esc_html__( 'Posts navigation', 'canopee' ) . '</h3>';

	echo '<div class="nav-links grid grid-cols-2 gap-10">';

		echo '<div class="nav-previous">';

			previous_post_link( sprintf( '<span class="h4-like">%s</span>', esc_html__( 'Previous post', 'canopee' ) ) . '%link', '%title' );

		echo '</div>';

		echo '<div class="nav-next">';

			next_post_link(  sprintf( '<span class="h4-like">%s</span>', esc_html__( 'Next post', 'canopee' ) ) . '%link', '%title' );

		echo '</div>';

	echo '</div><!-- .nav-links -->';

	// Close Inner if there is one
	if ( $inner ) echo '</' . $parsed['inner']['wrap'] . '>' . "\n\n" ;

	// Close Container if there is one
	if ( $container ) echo '</' . $parsed['container']['wrap'] . '>' . "\n\n" ;

	// Close Wrap Container if there is one
	if ( $wrap_container ) echo '</' . $parsed['wrap_container']['wrap'] . '>' . "\n\n" ;

}

/**
 * Tif_loopable
 * TODO ARRAY CONTAINER / INNER
 */
function tif_post_share( $attr = array() ) {

	if ( tif_is_woocommerce_page() )
		return;

	$default_attr  = array(
		'wrap_container'	=> array(
			'wrap'				=> false,
			'attr'				=> array(),
			'additional'		=> array()
		),
		'container'			=> array(
			'wrap'				=> 'div',
			'attr'				=> array(
				'class'				=> 'no-print entry-share-links',
			),
			'additional'		=> array()
		),
		'inner'				=> array(
			'wrap'				=> false,
			'attr'				=> array(),
			'additional'		=> array()
		),
	);

	$parsed = tif_parse_args_recursive( $attr, $default_attr );

	$wrap_container	= $parsed['wrap_container']['wrap'] ? '<' . $parsed['wrap_container']['wrap'] . tif_parse_attr( $parsed['wrap_container']['attr'] ) . '>' : false ;
	$container	= $parsed['container']['wrap'] ? '<' . $parsed['container']['wrap'] . tif_parse_attr( $parsed['container']['attr'], $parsed['container']['additional'] ) . '>' : false ;
	$inner		= isset( $parsed['inner']['wrap'] ) && $parsed['inner']['wrap'] ? '<' . $parsed['inner']['wrap'] . tif_parse_attr( $parsed['inner']['attr'], $parsed['inner']['additional'] ) . '>' : false ;

	$share_order = tif_get_callback_enabled( tif_get_option( 'theme_post_share', 'tif_post_share_order', 'array' ) );

	if ( empty( $share_order) )
		return;

	// Share buttons layout
	$size				= tif_get_option( 'theme_post_share', 'tif_post_share_size', 'key' );
	$bg_color			= tif_get_option( 'theme_post_share', 'tif_post_share_colors,bgcolor', 'key' );
	if ( null != $bg_color )
		$bg_color = $bg_color != 'bg_network' ? ' has-tif-' . tif_esc_css( $bg_color ) . '-background-color ' : ' social' ;

	$bg_color_hover		= tif_get_option( 'theme_post_share', 'tif_post_share_colors,bgcolor_hover', 'key' );
	$bg_color_hover		= null != $bg_color_hover && $bg_color_hover == 'bg_network' ? ' social-hover' : null;

	// Share buttons links
	global $post;

	$share_URL		= urlencode( get_permalink() );
	$share_title	= htmlentities( get_the_title() );

	$is_thumbnail	= null != get_post_thumbnail_id( $post->ID ) ? wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'tif-large' ) : false ;
	$share_media	= $is_thumbnail ?  $is_thumbnail[0] : null ;

	$search			= array( '\t', '\n', '\r', CHR(13), CHR(10));
	$share_excerpt	= ( has_excerpt( $post->ID ) ) ? str_replace($search, '', strip_tags(get_the_excerpt())) : '' ;

	$share_js		= null;
	$target			= ' target="_blank" rel="nofollow noreferrer noopener"';

	// $share_links	= '<h2 class="share-title screen-reader-text">' . esc_html__( 'Share', 'canopee' ) . '</h2>
	$share_links	= '<h2 class="share-title">' . esc_html__( 'Share on social networks', 'canopee' ) . '</h2>

	<ul ' . tif_post_share_links_class( $size ) . '>'."\n";

	/**
	 * @link http://www.sharelinkgenerator.com/
	 * @link https://www.coderstool.com/share-social-link-generator
	 */
	$share_settings = array(
		'facebook'		=> 'https://www.facebook.com/sharer/sharer.php?u=' . $share_URL,
		'twitter'		=> 'https://twitter.com/intent/tweet?url=' . $share_URL . '&amp;text=' . $share_title,
		'linkedin'		=> 'https://www.linkedin.com/shareArticle?url=' . $share_URL . '&amp;title=' . $share_title,
		'viadeo'		=> 'https://www.viadeo.com/?url=' . $share_URL . '&amp;title=' . $share_title,
		'pinterest'		=> 'https://pinterest.com/pin/create/button/?url=' . $share_URL . '&amp;media=' . $share_media . '&amp;description='.$share_excerpt,
		// 'scoopit'	=> 'https://www.scoop.it/oexchange/share?url=' . $share_URL . '&title=' . $share_title,
		'envelope-o'	=> 'mailto:?subject=' . $share_title . '&amp;body=' . $share_URL
	);

	foreach ( $share_order as $key => $value ) {

		$share_links .= '<li class="' . esc_attr( $key . $bg_color . $bg_color_hover ) . '"><a href="' . esc_url( $share_settings[$key] ) . '" ' . $share_js . $target . ' ><i class="fa fa-' . esc_html( $key ) . '" aria-hidden="true"></i></a></li>'."\n";

	}
	unset( $share_order );

	$share_links .= '</ul>'."\n";

	// Open Wrap Container if there is one
	if ( $wrap_container ) echo $wrap_container . "\n\n" ;

	// Open Container if there is one
	if ( $container ) echo $container . "\n\n" ;

	// Open Inner if there is one
	if ( $inner ) echo $inner . "\n\n" ;

	echo $share_links;

	// Close Inner if there is one
	if ( $inner ) echo '</' . $parsed['inner']['wrap'] . '>' . "\n\n" ;

	// Close Container if there is one
	if ( $container ) echo '</' . $parsed['container']['wrap'] . '>' . "\n\n" ;

	// Close Wrap Container if there is one
	if ( $wrap_container ) echo '</' . $parsed['wrap_container']['wrap'] . '>' . "\n\n" ;

}

/**
 * Tif_loopable
 * TODO
 */
function tif_post_related( $attr = array() ) {

	$loop_settings		= tif_get_option( 'theme_post_related', 'tif_post_related_settings', 'array' ) ;
	$loop_attr			= tif_get_loop_attr( $loop_settings['layout'] );
	$loop_attr			= tif_parse_loop_attrs( $attr, $loop_attr );

	$entry_order		= tif_get_option( 'theme_post_related', 'tif_post_related_entry_order', 'array' );
	$entry_enabled		= tif_get_callback_enabled( $entry_order );

	$meta_order		= tif_get_option( 'theme_post_related', 'tif_post_related_meta_order', 'array' ) ;
	$meta_enabled	= tif_get_callback_enabled( $meta_order );

	global $post;

	switch ( $loop_settings['content'] ) {
		case 'category':
			// Posts in the same category
			$args=array(
				'cat'					=> wp_get_post_categories( $post->ID ),
				'posts_per_page'		=> (int)$loop_settings['posts_per_page'],
				'ignore_sticky_posts'	=> 1,
				'orderby'				=> 'date',
				'order'					=> 'DESC'
			);
		break;

		default:
			// Related posts with tags
			$tag_id = array();
			$tags	= wp_get_post_tags( $post->ID );
			foreach ( $tags as $individual_tag ) $tag_id[] = $individual_tag->term_id;
			$args = array(
				'tag__in'				=> $tag_id,
				'post__not_in'			=> array( $post->ID ),
				'posts_per_page'		=> (int)$loop_settings['posts_per_page'],
				'ignore_sticky_posts'	=> 1
			);
			break;
	}

	$entry_settings = array(
		'post_thumbnail'	=> array(
			'loop'				=> 'post_related',
			'size'				=> $loop_attr['thumbnail']['size'],
		),
		'post_title_loop'	=> array(),
		'post_meta'			=> array(
			'callback'			=> $meta_enabled
		),
		'post_content'		=> array(
			'loop'				=> 'post_related',
			'length'			=> (string)$loop_settings['excerpt_length'],
		),
		'post_tags'			=> array(),
		'post_read_more'		=> array()
	);

	foreach ( $entry_enabled as $key => $value ) {
		$entry_enabled[$key] = $entry_settings[$key];
	}

	$related_attr = array(
		'title_loop'	=> array(
			'title'			=> esc_html__( 'Post Related', 'canopee' ),
			'wrap'			=> 'h2',
			'attr'			=> array( 'class' => 'title-loop ' . tif_esc_css( $loop_attr['title']['attr']['class'] ) ),
			'before'		=> '',
			'after'			=> '',
		),
		'container'		=> array(
			'wrap'			=> 'section',
			'additional'	=> array( 'class' => 'posts-list related-loop ' ),
		),
	);

	$parsed = tif_parse_args_recursive( $related_attr, $loop_attr );

	if ( $parsed['layout']  == 'row' || $parsed['layout']  == 'row_50' ) {

		// Unset thumbnail from entry order if row layout
		unset( $entry_enabled['post_thumbnail'] );

		$content_header = array(
			'post_thumbnail' => array(
				'loop'			=> 'post_related',
				'size'			=> $parsed['thumbnail']['size'],
				'container'		=> array(
					'wrap'			=> 'div',
					'attr'			=> array( 'class' => ' entry-thumbnail ' . tif_esc_css( $parsed['thumbnail']['attr']['class'] ) . get_post_format() )
				)
			),
		);

		$wrap_entry = array(
			'wrap_entry'	=> array(
				'loop'			=> 'post_related',
				'callback'		=> $entry_enabled,
				'container'		=> array(
					'wrap'			=> 'div',
					'attr'			=> array( 'class' => tif_esc_css( $parsed['post']['wrap_content']['attr']['class'] ) )
				),
			),
		);

		if ( isset( $parsed['hook'] ) && $parsed['hook'] == 'site_content_bottom' && $parsed['container']['grid'] == (int)1 )
			$parsed['post']['additional']['class']	.= ' unboxed';

	} else {

		$content_header = array();
		$wrap_entry = $entry_enabled;

	}

	$callback = array_merge( $content_header, $wrap_entry );

	tif_posts_query( $args, $callback, $parsed );

}
