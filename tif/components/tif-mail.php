<?php

if ( ! defined( 'ABSPATH' ) ) exit;

function tif_mail( $to, $subject, $message, $headers = null ) {

	// Unique boundary
	$boundary = md5( uniqid( "", true ) . microtime() );

	// headers
	$_headers[] = 'MIME-Version: 1.0';
	$_headers[] = 'Content-Type: multipart/alternative; boundary="' . $boundary . '"';

	// If $header sent
	if ( ! empty( $headers ) )
		$_headers[] = $headers;

	$_headers[] = 'X-Mailer: PHP/' . phpversion();

	// Plain text version of message
	$_body[] = '--' . $boundary;
	$_body[] = 'Content-Type: text/plain; charset="utf-8"';
	// $_body[] = 'Content-Transfer-Encoding: base64' . "\r\n";
	// $_body[] = chunk_split( base64_encode( strip_tags( $message ) ) );
	$_body[] = 'Content-Transfer-Encoding: 7bit' . "\r\n";
	$_body[] = chunk_split( strip_tags( $message ) );

	// HTML version of message
	$_body[] = '--' . $boundary;
	$_body[] = 'Content-Type: text/html; charset="utf-8"';
	// $_body[] = 'Content-Transfer-Encoding: base64' . "\r\n";
	$_body[] = 'Content-Transfer-Encoding: 7bit' . "\r\n";
	$_body[] = '<!DOCTYPE html>';
	$_body[] = '<html>';
	$_body[] = '<head><meta http-equiv="Content-Type" content="text/html charset=UTF-8" /></head>';
	$_body[] = '<body>';
	// $_body[] = chunk_split( base64_encode( nl2br( $message ) ) );
	$_body[] = chunk_split( nl2br( $message ) );
	$_body[] = '</body></html>'."\r\n";
	$_body[] = '--' . $boundary . '--';

	$header = implode("\r\n", $_headers);
	$body   = implode("\r\n", $_body);

	if ( is_array( $to ) ) {

		$mail_result = array();
		foreach ( $to as $mailto ) {

			$mail_sent = filter_var( trim( $mailto ), FILTER_VALIDATE_EMAIL ) ? @mail( trim( $mailto ), $subject, $body, $header ) : false ;
			$mail_result = array_merge( $mail_result , array( $mailto => $mail_sent ) );

		}

	} else {

		$to = explode( ",", $to );
		$mailto = array();
		foreach ( $to as $mail ) {

			if ( filter_var( trim( $mail ), FILTER_VALIDATE_EMAIL ) )
				$mailto[] =  trim( $mail ) ;

		}
		$to = implode( ',', $mailto );

		$mail_sent = null != $to ? @mail( $to, $subject, $body, $header ) : false ;
		$mail_result = array( $to => $mail_sent );

	}

	return $mail_result;

}

function tif_mail_copy( $copy, $type = null ) {

	// Add Cc and Bcc to header
	$copy = ! is_array( $copy ) ? explode( ",", $copy ) : $copy;

	$mailcopy = array();
	foreach ( $copy as $mail ) {

		if ( filter_var( trim( $mail ), FILTER_VALIDATE_EMAIL ) )
			$mailcopy[] =  trim( $mail ) ;

	}

	$type = $type == 'bcc' ? 'Bcc' : 'Cc' ;
	if ( ! empty( $mailcopy) )
		$_headers = $type . ': ' . implode( ',', $mailcopy ) . "\r\n";

	return $_headers;

}
