<?php

if ( ! defined( 'ABSPATH' ) ) exit;

require_once( ABSPATH . 'wp-admin/includes/file.php' );

WP_Filesystem();

use ScssPhp\ScssPhp\Compiler;
use ScssPhp\ScssPhp\ValueConverter;
function tif_compile_scss( $args = array() ) {

	$default_args  = array(
		'origin'		=> 'stylesheet',
		'components'	=> array(),
		'variables'		=> array(),
		'path'			=> null,
		'output'		=> array(
			'abspath'		=> false,
			'path'			=> null,
			'name'			=> null,
		),
	);

	$parsed = tif_parse_args_recursive( $args, $default_args );

	if ( empty( $parsed['components'] ) || null ==  $parsed['path'] )
		return;

	require_once 'scssphp/scss.inc.php';
	global $wp_filesystem;

	$css = null;
	foreach ( $parsed['components'] as $key ) {

		if ( $parsed['origin'] == 'abspath' || $parsed['origin'] == 'plugin' ) :
			$scssContents = $wp_filesystem->get_contents( $parsed['path']. '/' . $key . '.' . 'scss' );
			$import_path  = $parsed['path'];

		else :
			// Origin (template or stylesheet)
			$get_assets   = 'tif_get_' . $parsed['origin'] . '_assets';
			$scssContents = $wp_filesystem->get_contents( $get_assets( $parsed['path'] . '/', $key, 'scss' ) );
			$import_path  = get_template_directory() . $parsed['path'];

		endif;

		if ( is_bool( $parsed['output']['abspath'] ) && $parsed['output']['abspath'] ) :
			$cssTarget = $parsed['output']['path'];

		else :
			$cssTarget = get_template_directory() . $parsed['output']['path'] . '/';

		endif;


		$variables = array();
		$compiler = new Compiler();
		$compiler->addImportPath( $import_path );

		foreach ( $parsed['variables'] as $key => $value ) {

			if ( null != $value )
				$variables[$key] = ValueConverter::parseValue( $value );

			else
				$variables[$key] = ValueConverter::fromPhp( $value );

		}

		// DEBUG:
		// tif_print_r($variables);

		$compiler->replaceVariables( $variables );
		$tmp = $compiler->compileString( $scssContents )->getCss();

		if ( ! empty( $tmp ) && is_string( $tmp ) && null != $parsed['output']['path'] && null == $parsed['output']['name'] ) :
			tif_create_assets(
				array (
					'content'	=> $tmp,
					'type'		=> 'css',
					'path'		=> $cssTarget,
					'name'		=> $key
				)
			);
		else:
			$css .= $tmp;
		endif;

	}

	if ( ! empty( $css ) && is_string( $css ) && null != $parsed['output']['path'] && null != $parsed['output']['name'] ) :

		tif_create_assets(
			array (
				'content'	=> $css,
				'type'		=> 'css',
				'path'		=> $cssTarget,
				'name'		=> $parsed['output']['name']
			)
		);
	else:
		return $css;
	endif;

}

function tif_concat_assets( $args = array() ) {

	$default_args  = array(
		'origin'		=> 'stylesheet',
		'components'	=> array(),
		'type'			=> 'css',
		'path'			=> null,
		'output'		=> array(
			'path'			=> null,
			'name'			=> null
		),
	);

	$parsed = tif_parse_args_recursive( $args, $default_args );

	if ( empty( $parsed['components'] ) || null == $parsed['path'] )
		return;

	$parsed['components'] = ! is_array( $parsed['components'] ) ? explode( ',', $parsed['components'] ) : $parsed['components'] ;

	global $wp_filesystem;

	// Prevent a notice
	$concat = '';


	// Loop the css Array
	foreach ( $parsed['components'] as $key ) {
		if ( $parsed['origin'] == 'abspath' || $parsed['origin'] == 'plugin' ) :

			$path = null != $parsed['path'] ? $parsed['path'] . '/' : null;
			$concat .= $wp_filesystem->get_contents( $path . $key . '.' . $parsed['type'] );
			$concatTarget = $parsed['output']['path'];

		else :

			// Origin (template or stylesheet)
			$get_assets	= 'tif_get_' . $parsed['origin'] . '_assets';

			$concat .= $wp_filesystem->get_contents( $get_assets( $parsed['path'] . '/', $key, $parsed['type'] ) );
			$concatTarget = get_template_directory() . $parsed['output']['path'];

		endif;
	}

	if ( ! empty( $concat ) && is_string( $concat ) && null != $parsed['output']['path'] && null != $parsed['output']['name'] ) :
		tif_create_assets(
			array (
				'content'	=> $concat,
				'type'		=> $parsed['type'],
				'path'		=> $concatTarget,
				'name'		=> $parsed['output']['name']
			)
		);
	else:
		return $concat;
	endif;

}

function tif_create_assets( $args = array() ) {

	$defaults  = array(
		'content'		=> null,
		'type'			=> 'css',
		'path'			=> null,
		'name'			=> null
	);

	$parsed = wp_parse_args( $args, $defaults );

	if ( empty( $parsed['content'] ) || null ==  $parsed['path'] || null ==  $parsed['name'] )
		return;

	global $wp_filesystem;

	$minimized = $parsed['type'] == 'css' ? 'tif_minimize_css' : 'tif_minimize_js' ;

	$wp_filesystem->put_contents(
		$parsed['path'] . '/' . $parsed['name'] . '.min.' . $parsed['type'],
		$minimized( $parsed['content'] ),
		FS_CHMOD_FILE
	);

	$wp_filesystem->put_contents(
		$parsed['path'] . '/' . $parsed['name'] . '.' . $parsed['type'],
		$parsed['content'],
		FS_CHMOD_FILE
	);

}
