<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Get Tweaks setup data
 */

 // Masquer les erreurs de connexion											error_connexion
 // Retirer les caractères spéciaux des medias téléversées						tools_sanitize_file_name
 // Retirer la version de WordPress												wp_version
 // Supprimer le manifeste de Windows Live Writer								link_manifest
 // Restreindre le nombre de révision											deactivate_revisions
 // Retirer H1 dans Tiny MCE													delete_h1_editor
 // Désactiver les Emoji WordPress												disable_emoji
 // Désactiver l'API REST														deactivate_rest_api
 // Ajouter le format d’image Medium Large										add_medium_large
 // Désactiver la page auteur et le lien auteur									deactivate_author_page_and_link
 // Supprimer le flux RSS														feed_link
 // Supprimer le flux RSS des commentaires										comments_feed
 // Désactiver les utilisateurs du sitemap WordPress par défaut					checked_users_remove_sitemap
 // Désactiver les endpoints utilisateur REST API								disable_rest_api_users

function tif_plugin_tweaks_setup_data() {

	return $tif_tweaks_setup_data = array(

		'tif_tabs'					=> 1,
		'tif_template_setup_data'	=> null,

		// Settings
		'tif_init'					=> array(
			'unify_menus'				=> null,
			'capabilities'				=> null,
			'restrictions'				=> null,
			'admin_footer'				=> null,
		),

		'tif_customizer'			=> array(
			'globals'					=> null,
			'settings'					=> null,
			'additional'				=> null,
			'woo'						=> null,
			'template'					=> null,
			'homepage_control'			=> null,
			'restricted'				=> array(
				'theme_utils'
			),
		),

		// Disabled Callback
		'tif_callback'				=> array(
			// Front
			'remove_jquery'				=> null,
			'remove_emojis'				=> null,
			'wp_generator'				=> null,
			'wlwmanifest_link'			=> null,
			// 'adjacent_links'			=> null,
			'shortlink'					=> null,
			'disable_rest_api'			=> null,
			'disable_rest_api_users'	=> null,
			// 'recent_comments_style'		=> null,

			'close_comments'			=> array(
				// 'post',
				// 'page',
				// 'attachment',
			),
			'noindex'					=> null,
			'author_disabled'			=> null,
			'author_status'				=> 404,
			'invert_comments_fields'	=> null,
			'hide_url_field'			=> null,
			'remove_comments_class'		=> array(
				// 'byuser',
				// 'bypostauthor',
				'commentauthor',
			),

			// Back
			'remove_wp_logo'			=> null,
			'remove_dashboard'			=> null,
			'login_page_logo'			=> null,
			'login_page_bgimg'			=> null,
			'login_page_bgcolor'		=> null,
			'lost_password_link'		=> null,
			'hide_login_errors'			=> null,

			// Media
			'sanitize_uploads'			=> null,
			'images_sizes'				=> null,

			// Theme support
			'removed_theme_support'		=> null
		),

		// Restricted Disabled Callback
		'tif_restricted_callback'	=> array(
			'hide_core_updates'			=> null,
		),

		// 'tif_comments'				=> array(
		// 	'close'						=> array(
		// 		// 'post',
		// 		// 'page',
		// 		// 'attachment',
		// 	),
		// ),

		// Feeds
		'tif_feeds'					=> array(
			'main'						=> null,
			'author'					=> null,
			'category'					=> null,
			'search'					=> null,
			'tag'						=> null,
			'comments'					=> null,
			'posts_comments'			=> null,
		)

	);

}
